$(document).ready(function () {
    var lazyLoadInstance = new LazyLoad({
        elements_selector: ".lazy"
        // ... more custom settings?
    });

    // animate on scroll
    AOS.init({
        easing: 'ease-in-out-sine'
    });

    // Accounts for the browser bar in mobile
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);

    // We listen to the resize event
    window.addEventListener('resize', () => {
        // We execute the same script as before
        let vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    });

    if ($('#displayPts')) {
        var modalPts = $('#displayPts');
        var modalOpenButtonPts = $('.openPtsModal');
        var modalContentPts = $('.modal-content');
        var closePts = $('.close');
        modalOpenButtonPts.click(function () {
            // Setting the modal contents...
            if ($('#pts-modal-heading')) {
                $('#pts-modal-heading').html($(this).attr('data-heading'));
            }


            if ($('#ptsImage')) {
                // console.log($(this).attr('data-instructor-image'));
                $('#ptsImage').attr('src', $(this).attr('data-instructor-image'));
            }
            if ($('#pts-abstract')) {

                $('#pts-abstract').html($(this).attr('data-abstract'))
            }
            if ($('#pts-description')) {

                $('#pts-description').html($(this).attr('data-description'));
            }
            modalPts.fadeIn();
        });

        // When user clicks outside of modal, close it or when they click close
        closePts.click(function (e) {
            modalPts.fadeOut();
        });
        modalPts.click(function (e) {
            modalPts.fadeOut();
        });
        // Prevent user clicking contents of modal and accidentally closing the modal
        modalContentPts.click(function (e) {
            e.stopPropagation();
        })
    }

    if ($('.js-mega-btn')[0]) {
        var active = false;
        console.log(active);
        $('.js-mega-btn').click(function (e) {
            e.preventDefault();
            if (!active) {
                $(this).addClass('nav-current');
                active = !active;
                $('.js-mega-menu').show();
                $('body').css('overflow', 'hidden');
            } else {
                active = !active;
                $('.js-mega-menu').fadeOut();
                $(this).removeClass('nav-current');
                $('body').css('overflow', 'unset');
            }
            $('.js-mega-menu').click(function () {
                active = !active;
                $('.js-mega-btn').removeClass('nav-current');
                $('.js-mega-menu').fadeOut();
                $('body').css('overflow', 'unset');
            });
        });

    }

    // Accordion
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        console.log("test");
        acc[i].addEventListener("click", function () {
            console.log("clicked");
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }

    // Sidebar 

    if ($('.js-side-menu')[0]) {
        $('.js-side-menu').click(function () {
            $('.side-bar').toggleClass('open');
            $('.js-overlay').toggle();
            $('.js-side-menu').toggleClass('open');
        });
    }


    // triggle mobile menu 
    if ($('.js-mobile-menu')[0]) {
        var active = false;
        $('.js-mobile-menu').click(function () {
            $('.js-mobile-menu-view, .pattern').removeClass('animationReset');
            if (!active) {
                active = !active;
                $('.js-mobile-menu-view').show();
            } else {
                active = !active;
                $('.js-mobile-menu-view, .pattern').addClass('animationReset');
                $('.js-mobile-menu-view').fadeOut('slow');
                $('.js-services-btn, .services').removeClass('open');
            }
        });
    }

    // trigger services menu 
    if ($('.js-services-btn')[0]) {
        $('.js-services-btn').click(function () {
            $('.services').toggleClass('open');
            $('.js-services-btn').toggleClass('open');
        });
    };


    if ($('.swiper-classInfo-new')[0]) {
        var swiper = new Swiper('.swiper-classInfo-new', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: false,
            autoplay: {
                delay: 5000,
            },

            // breakpoints: {
            //     // when window width is <= 499px
            //     1450: {
            //         slidesPerView: 1,
            //         spaceBetweenSlides: 50
            //     },
            //     // when window width is <= 999px
            //     999: {
            //         slidesPerView: 2,
            //         spaceBetweenSlides: 50
            //     }
            // },
            navigation: {
                nextEl: '.swiper-button-infoBlock-next',
                prevEl: '.swiper-button-infoBlock-prev',
            },
            // Enable lazy loading
            lazy: true,
        });
    }

    // test

    var galleryThumbs = new Swiper('.js-board-swiper2', {
        spaceBetween: 10,
        slidesPerView: 3,
        slidesPerColumn: 2,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        direction: 'vertical',
    });
    var galleryTop = new Swiper('.js-board-swiper', {
        spaceBetween: 10,
        slidesPerView: 1,
        loop: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });
});

(function () {
    // MB - Credit David Walsh (https://davidwalsh.name/javascript-debounce-function). Use a debounce with all onscrolls if it will be fired a lot. Dont have to use this, but do consider it
    $(window).scroll(function () {

        var distanceFromTop = $(this).scrollTop();
        // this.console.log(distanceFromTop);
        if (distanceFromTop > 400) {
            $('.js-sticky-nav').addClass('fixed-nav');
        } else {
            $('.js-sticky-nav').removeClass('fixed-nav');
        }
    });
})();

// Vue being used for contact form 
Vue.options.delimiters = ['@{', '}'];

// Text area component - Check user has added some content and add ticks where they have
let textArea = {
    // Example usage:
    // input-field(placeholder="Full name" label="Name" name="message[contactName][]" type="text" required=true)
    props: {
        name: String,
        placeholder: String,
        label: String,
        id: String,
        required: {
            type: Boolean,
            default: false
        }
    },
    template: `
        <div v-bind:class="activeClass" class="textarea-row">
            <div class="label-cont">
                <label v-bind:for="id">@{label}</label>
            </div>

            <transition-group name="fade">
                <img v-if="valid == 'partial'" src="${baseUrl}build/images/tick-primary.svg" alt="tick-primary" class="tick"  :key="1"/>
                <img v-if="valid == 'valid'" src="${baseUrl}build/images/tick-secondary.svg" alt="tick-secondary" class="tick"  :key="2"/>
            </transition-group>
            <textarea
                :id="id"
                v-model="value"
                :name="name"
                :required="required"
                :placeholder="placeholder"
            />

        </div>
    `,
    data: function () {
        return {
            valid: 'invalid',
            value: ''
        }
    },
    computed: {
        activeClass: function () {
            if (this.valid == 'valid') {
                return 'inputActive';
            }

        }
    },
    watch: {
        value: function (val) {
            if (val.length < 1) {
                this.valid = 'invalid';
            } else if (val.length >= 1 && val.length < 3) {
                this.valid = 'partial';
            } else if (val.length >= 3) {
                this.valid = 'valid';
            }
        }
    }
}
// Input field validation. Check user has added some content when using input fields - If they have create a tick next to the word
let inputField = {
    // Example usage:
    // input-field(placeholder="Full name" label="Name" name="message[contactName][]" type="text" required=true)
    props: {
        name: {
            type: String,
        },
        placeholder: {
            type: String
        },
        type: {
            type: String,
            default: 'text'
        },
        label: {
            type: String,
        },
        required: {
            type: Boolean,
            default: false
        }
    },
    template: `
        <div v-bind:class="activeClass">
            <div class="input-row">
                <div class="label-cont">
                    <label v-bind:for="placeholder">@{label}</label>
                </div>
                <transition-group name="fade">
                    <img v-if="valid == 'partial'" src="${baseUrl}build/images/tick-primary.svg" alt="tick-primary" class="tick"  :key="1"/>
                    <img v-if="valid == 'valid'" src="${baseUrl}build/images/tick-secondary.svg" alt="tick-secondary" class="tick"  :key="2"/>
                </transition-group>
                <input
                    :id="placeholder"
                    :type="type"
                    v-model="value"
                    :placeholder="placeholder"
                    :name="name"
                    :required="required"
                />
            </div>
        </div>
    `,
    data: function () {
        return {
            valid: 'invalid',
            value: ''
        }
    },
    computed: {
        activeClass: function () {
            if (this.valid == 'valid') {
                return 'inputActive';
            }

        }
    },
    watch: {
        value: function (val) {
            if (val.length < 1) {
                this.valid = 'invalid';
            } else if (val.length >= 1 && val.length < 3) {
                this.valid = 'partial';
            } else if (val.length >= 3) {
                this.valid = 'valid';
            }
        }
    }
}
// Select field validation. Check user has added some content when using select fields - If they have create a tick next to the word
let selectField = {
    // Example usage:
    props: {
        name: String,
        id: String,
        label: [String, Number],
        'placeholder': {
            type: String,
            default: 'Please select one'
        },
        options: {
            type: Array
        },
        required: {
            type: Boolean,
            default: false,
        }
    },

    template: `
        <div v-bind:class="activeClass">
            <div class="input-row selectFieldBlock">
                <div class="label-cont">
                    <label v-bind:for="id">@{label}</label>
                </div>
                <transition-group name="fade">                    
                    <img v-if="valid == 'valid'" src="${baseUrl}build/images/tick-secondary.svg" alt="tick-secondary" class="tick"  :key="2"/>
                </transition-group>
                <select 
                    :id="id"
                    :name="name"
                    v-model="value"
                    :required="required">                                                   
                    <option disabled value="">@{placeholder}</option>
                    <option v-for="individualOption in options">@{individualOption}</option>
                </select>
            </div>
        </div>
    `,
    data: function () {
        return {
            valid: 'invalid',
            value: ''
        }
    },
    computed: {
        activeClass: function () {
            if (this.valid == 'valid') {
                return 'inputActive';
            }

        }
    },
    watch: {
        value: function (val) {

            if (val.length > 1) {
                this.valid = 'valid';
            }

        }
    }
}
// Email Field component - Check user has added some content and that its a valid email format with Regex - add ticks where they have
let emailField = {
    // Example usage:
    // input-field(placeholder="Full name" label="Name" name="message[contactName][]" required=true)
    props: {
        name: String,
        placeholder: String,
        label: String,
        required: {
            type: Boolean,
            default: false
        }
    },
    template: `
        <div v-bind:class="activeClass">
            <div class="label-cont">
                <label v-bind:for="placeholder">@{label}</label>
            </div>
            <div class="input-row email">

                <transition-group name="fade">
                    <img v-if="valid == 'partial'" src="${baseUrl}build/images/tick-primary.svg" alt="tick-primary" class="tick"  :key="1"/>
                    <img v-if="valid == 'valid'" src="${baseUrl}build/images/tick-secondary.svg" alt="tick-secondary" class="tick"  :key="2"/>
                </transition-group>
                <input
                    :id="placeholder"
                    v-model="value"
                    :placeholder="placeholder"
                    :name="name"
                    :required="required"
                    type="email"
                />
            </div>
        </div>
    `,
    data: function () {
        return {
            valid: 'invalid',
            value: '',
        }
    },
    computed: {
        activeClass: function () {
            if (this.valid == 'valid') {
                return 'inputActive';
            }
        }
    },
    methods: {
        checkValidEmail: function (val) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(val)) {
                return (true)
            }
            // console.log("You have entered an invalid email address!")
            return (false)
        }
    },
    watch: {
        value: function (val) {
            if (val.length < 1) {
                this.valid = 'invalid';
            } else if (val.length >= 3) {
                this.valid = 'partial';
                if (this.checkValidEmail(val)) {
                    this.valid = 'valid';
                }

            }
        },
    }
}
// New vue instance. Add required components to global instance.
var app = new Vue({
    //#app is the vue instance mounted to the DOM
    el: '#app',

    components: {
        'input-field': inputField,
        'email-field': emailField,
        'select-field': selectField,
        'text-area': textArea,
        'datepicker': vuejsDatepicker
    },
    data: function () {
        return {
            show: true,
            news: [],
            newsLimit: 6,
            newsLoading: true,
            newsTotal: 0,
            startDate: '',
            endDate: ''
        }
    },
    methods: {
        toggleNewsLoading: function (bool) {
            this.newsLoading = bool;
        },
        loadMoreNews: function () {
            if (window.innerWidth > 1100) {
                this.newsLimit += 3;
            } else {
                this.newsLimit += 2;
            }
        },
    },
    computed: {
        // if no results, feedback to user
        newsList: function () {
            if (this.newsHasData) {
                // if dates are not selected
                if (!this.datesSelected) {
                    this.toggleNewsLoading(true);
                    // return original array
                    var newArray = this.news.data;
                    this.toggleNewsLoading(false);
                    return newArray;
                } else {
                    // with dates selected
                    this.toggleNewsLoading(true);
                    var newArray = [];
                    // loop through each item
                    // look at map functions
                    this.news.data.forEach((item) => {
                        var dateNew = item.postDate.date.split(' ')[0];
                        var newsDate = new Date(dateNew);
                        // check if entry is between the two selected dates
                        if (newsDate >= this.startDate && newsDate <= this.endDate) {
                            // push to new array
                            newArray.push(item);
                        }
                    });
                    this.toggleNewsLoading(false);
                    return newArray;
                }
            }
        },
        newsRemaining: function () {
            if (this.newsHasData) {
                if (this.newsLimit > this.newsTotal) {
                    return false;
                }
            }
            return true;
        },
        newsDataLimited: function () {
            // if there is data, limit with slice to by the news limit property
            if (this.newsHasData && !this.datesSelected) {
                return this.newsList.slice(0, this.newsLimit);
            } else {
                // limit it to so many
                return this.newsList;
            }
            return [];
        },
        newsHasData: function () {
            // check if there is data in the news property
            if (this.news.data) {
                return true;
            }
            return false;
        },
        datesSelected: function () {
            // check if dates have been selected
            // could filter by only start date - to a set date say a months time
            if (this.startDate === '' || this.endDate === '') {
                return false;
            } else {
                return true;
            }
        }
    },
    mounted() {
        // curl requests, search .json in rpl
        // getting news articles from the api
        axios.get(window.baseUrl + 'news.json')
            .then(response => {
                // set the api to the news property
                this.news = response.data;
                this.newsTotal = this.news.meta.pagination.total;
                console.log('Display news articles component has now been mounted and the Axios call to ' + window.baseUrl + 'news.json' + ' news.json was successful, see below:');
                console.log(this.news);
            });
    },
});

// Kieran Vue //
// steps

// 1. create element API - include axios library
// 2. Pull into Vue using axios
// 3. Display the news articles

// get date from user input
// get date from 
// create array of filtered articles


//Matching header height
$(document).ready(function () {
    if ($('.matchHeightWrap')[0]) {
        // Select and loop the container element of the elements you want to equalise
        function matchHeightResize() {

            $('.matchHeightWrap').each(function () {
                // Cache the highest
                var highestBox = 0;

                // Select and loop the elements you want to equalise
                $('.matchHeightColumn', this).each(function () {
                    $(this).height('auto')
                    // If this box is higher than the cached highest then store it
                    if ($(this).height() > highestBox) {
                        highestBox = $(this).height();
                    }

                });

                if ($(window).width() > 850) {
                    // Set the height of all those children to whichever was highest 
                    $('.matchHeightColumn', this).height(highestBox);
                }

            });


        }
        matchHeightResize();

        $(window).resize(function () {
            matchHeightResize();
        });
    }
});