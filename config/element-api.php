<?php

use craft\elements\Entry;
use craft\helpers\UrlHelper;
use aelvan\imager\variables\ImagerVariable;
use craft\elements\MatrixBlock;

// $criteria = ['section' => 'news'];

return [
    'endpoints' => [
        'news.json' => function() {
            return [
                'elementType' => Entry::class,
                'criteria' => ['section' => 'news'],
                'transformer' => function(Entry $entry) {
                    $imager = new ImagerVariable();
                    $mainImage = $entry->mainImage->all();
                    $mainImageTransformImages = [];
                    foreach ($mainImage as $key => $image) {
                        $mainImageTransformImages [] = [
                            // 'placeholder' => 'https://www.etfscapital.com/' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),
                            // 'url' => 'https://www.etfscapital.com/' . $imager->transformImage( $image , ['width' => '1500', 'blur' => true ] )->getUrl(),
                            // 'placeholder' => 'http://ipop.je/give2020/' . $imager->transformImage( $image , [ 'width' => '50', 'blur' => true ] )->getUrl(),
                            // 'url' => 'http://ipop.je/give/web' . $imager->transformImage( $image , [ 'width' => '1500', 'blur' => true ] )->getUrl()
                            'placeholder' => 'http:/localhost/give/web' . $imager->transformImage( $image , [ 'width' => '6', 'blur' => true ] )->getUrl(),
                            'url' => 'http://localhost/give/web' . $imager->transformImage( $image , [ 'width' => '1500', 'blur' => true ] )->getUrl()
                        ];
                    }           
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'jsonUrl' => UrlHelper::url("news/{$entry->id}.json"),
                        'postDate' => $entry->postDate,
                        'images' => $mainImageTransformImages,
                    ];
                },
            ];
        },
        'news/<entryId:\d+>.json' => function($entryId) {
            return [
                'elementType' => Entry::class,
                'criteria' => ['id' => $entryId],
                'one' => true,
                'transformer' => function(Entry $entry) {
                    return [
                        'title' => $entry->title,
                        'url' => $entry->url,
                        'body' => $entry->body,
                    ];
                },
            ];
        },
    ]
];  