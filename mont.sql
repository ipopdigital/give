-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 30, 2019 at 03:43 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mont`
--

-- --------------------------------------------------------

--
-- Table structure for table `assetindexdata`
--

CREATE TABLE `assetindexdata` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

CREATE TABLE `assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`id`, `volumeId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(6, 1, 1, 'placeholder.png', 'image', 640, 360, 5531, NULL, NULL, NULL, '2019-08-30 13:41:24', '2019-08-30 13:41:24', '2019-08-30 13:41:28', 'f26abc6c-3dd2-4c3d-8d7f-9879d8f17c08');

-- --------------------------------------------------------

--
-- Table structure for table `assettransformindex`
--

CREATE TABLE `assettransformindex` (
  `id` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `assettransforms`
--

CREATE TABLE `assettransforms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups`
--

CREATE TABLE `categorygroups` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorygroups_sites`
--

CREATE TABLE `categorygroups_sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_addressOne` text,
  `field_addressTwo` text,
  `field_email` text,
  `field_facebook` text,
  `field_instagram` text,
  `field_openHours` text,
  `field_telephone` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`, `field_addressOne`, `field_addressTwo`, `field_email`, `field_facebook`, `field_instagram`, `field_openHours`, `field_telephone`) VALUES
(1, 1, 1, NULL, '2019-08-30 10:53:11', '2019-08-30 10:53:11', 'f1798bf7-c1c4-4e4f-8c8c-d889a696f9bd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 1, 'Home', '2019-08-30 10:57:24', '2019-08-30 13:41:47', '28b985fb-8da4-474c-af38-efdcc06f1db1', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 1, 'Home', '2019-08-30 10:57:24', '2019-08-30 10:57:24', 'f48a2884-049b-4c66-9d89-f9290ffa9953', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 1, 'Home', '2019-08-30 10:58:35', '2019-08-30 10:58:35', '836f1879-b707-48b1-bece-192c4ea686cf', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 1, NULL, '2019-08-30 11:06:33', '2019-08-30 11:06:33', 'f2ec3a1f-c753-4fb6-9f6a-05b68014afcd', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 1, 'Placeholder', '2019-08-30 13:41:24', '2019-08-30 13:41:28', '3320970a-cce3-4228-94ac-718c4549b148', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 1, 'Home', '2019-08-30 13:41:47', '2019-08-30 13:41:47', '65a91b3e-a1f3-4b24-adea-56dd9dd3467f', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `craftidtokens`
--

CREATE TABLE `craftidtokens` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `deprecationerrors`
--

CREATE TABLE `deprecationerrors` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) UNSIGNED DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `drafts`
--

CREATE TABLE `drafts` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elementindexsettings`
--

CREATE TABLE `elementindexsettings` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `elements`
--

CREATE TABLE `elements` (
  `id` int(11) NOT NULL,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements`
--

INSERT INTO `elements` (`id`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, NULL, NULL, 'craft\\elements\\User', 1, 0, '2019-08-30 10:53:11', '2019-08-30 10:53:11', NULL, '6b0b2abc-2786-4403-9cdf-104dc7de6a2d'),
(2, NULL, NULL, 1, 'craft\\elements\\Entry', 1, 0, '2019-08-30 10:57:24', '2019-08-30 13:41:47', NULL, '0e9157b9-53ae-4790-ac7e-3f7992913df2'),
(3, NULL, 1, NULL, 'craft\\elements\\Entry', 1, 0, '2019-08-30 10:57:24', '2019-08-30 10:57:24', NULL, 'ddcda886-770b-42be-a5df-75c6e31e2c96'),
(4, NULL, 2, NULL, 'craft\\elements\\Entry', 1, 0, '2019-08-30 10:58:35', '2019-08-30 10:58:35', NULL, '121b3520-4b9d-4dbf-8095-1ab9fad58e46'),
(5, NULL, NULL, 6, 'craft\\elements\\GlobalSet', 1, 0, '2019-08-30 11:06:33', '2019-08-30 11:06:33', NULL, '3de1d3c8-68e1-4cf0-93bf-751d4e0e6686'),
(6, NULL, NULL, NULL, 'craft\\elements\\Asset', 1, 0, '2019-08-30 13:41:24', '2019-08-30 13:41:28', NULL, '439c229f-1c15-4a03-98df-14287126c770'),
(7, NULL, 3, 1, 'craft\\elements\\Entry', 1, 0, '2019-08-30 13:41:47', '2019-08-30 13:41:47', NULL, '4fa73159-357e-45be-95c5-f98460badb32');

-- --------------------------------------------------------

--
-- Table structure for table `elements_sites`
--

CREATE TABLE `elements_sites` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `elements_sites`
--

INSERT INTO `elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, NULL, NULL, 1, '2019-08-30 10:53:11', '2019-08-30 10:53:11', '17e1fa37-35c5-45f4-be7c-a8de133be19a'),
(2, 2, 1, 'home', '__home__', 1, '2019-08-30 10:57:24', '2019-08-30 10:57:24', '091ea1b9-eee0-4dbe-90d9-3af907c5b951'),
(3, 3, 1, 'home', '__home__', 1, '2019-08-30 10:57:24', '2019-08-30 10:57:24', '9177f75f-b496-472c-ae4b-14d0f17a16f2'),
(4, 4, 1, 'home', '__home__', 1, '2019-08-30 10:58:35', '2019-08-30 10:58:35', 'e979953a-93d7-4ff4-8659-1c723971e34a'),
(5, 5, 1, NULL, NULL, 1, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '86747a03-8410-437c-bea8-a2461963d188'),
(6, 6, 1, NULL, NULL, 1, '2019-08-30 13:41:24', '2019-08-30 13:41:24', '60c4948e-6a05-4f1f-a60e-1de44c317adf'),
(7, 7, 1, 'home', '__home__', 1, '2019-08-30 13:41:47', '2019-08-30 13:41:47', 'df5cf1f3-e803-4a9f-be39-7fca9f40d08d');

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE `entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(2, 1, NULL, 1, NULL, '2019-08-30 10:57:00', NULL, NULL, '2019-08-30 10:57:24', '2019-08-30 10:57:24', 'eddcbb26-c224-4ff7-9e7b-68e362a194dd'),
(3, 1, NULL, 1, NULL, '2019-08-30 10:57:00', NULL, NULL, '2019-08-30 10:57:24', '2019-08-30 10:57:24', 'e32d914c-1b15-48c4-a2d7-dc0ed2b09e6a'),
(4, 1, NULL, 1, NULL, '2019-08-30 10:57:00', NULL, NULL, '2019-08-30 10:58:35', '2019-08-30 10:58:35', '54daca4b-7b13-4aa1-bf98-304aafabb46d'),
(7, 1, NULL, 1, NULL, '2019-08-30 10:57:00', NULL, NULL, '2019-08-30 13:41:47', '2019-08-30 13:41:47', 'c16e4215-1a87-4d07-a3ce-f83cb3297c9e');

-- --------------------------------------------------------

--
-- Table structure for table `entrytypes`
--

CREATE TABLE `entrytypes` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `entrytypes`
--

INSERT INTO `entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'Home', 'home', 0, '', '{section.name|raw}', 1, '2019-08-30 10:57:24', '2019-08-30 10:58:35', NULL, '2457c100-8550-4bc6-8888-a82f8977856f');

-- --------------------------------------------------------

--
-- Table structure for table `fieldgroups`
--

CREATE TABLE `fieldgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldgroups`
--

INSERT INTO `fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'Common', '2019-08-30 10:53:11', '2019-08-30 10:53:11', '340d14f1-feff-4fc1-852b-b499b39e9730'),
(2, 'Header', '2019-08-30 10:57:59', '2019-08-30 10:57:59', '57851f76-0174-4774-aa86-604b6c08e5df'),
(3, 'Contact', '2019-08-30 11:04:58', '2019-08-30 11:04:58', '50db6c0a-9c1b-49f4-8452-63157480a752');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayoutfields`
--

CREATE TABLE `fieldlayoutfields` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayoutfields`
--

INSERT INTO `fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, 0, 1, '2019-08-30 10:58:35', '2019-08-30 10:58:35', '35d8b756-730c-4645-9400-d14343464b8b'),
(19, 6, 7, 23, 0, 6, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '56f3e7d9-0857-4d08-a4eb-593266750109'),
(20, 6, 7, 22, 0, 8, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '7799addf-ace3-45ab-b207-e061f85fea9b'),
(21, 6, 7, 19, 0, 4, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '27e2b6fb-bcee-4d98-9f14-195f9464e374'),
(22, 6, 7, 18, 0, 3, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '3a0fc137-6301-4b5b-ad70-f267ced4d2fe'),
(23, 6, 7, 21, 0, 7, '2019-08-30 11:06:33', '2019-08-30 11:06:33', 'ac195d81-362a-4e4f-a5b4-2e5ee8ebe9c2'),
(24, 6, 7, 20, 0, 5, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '82d3bafa-7615-41c0-8be7-24eb724e8b96'),
(25, 6, 7, 16, 0, 1, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '7d719717-309f-4b15-ae51-f631519c90bc'),
(26, 6, 7, 17, 0, 2, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '2c3cf7fa-5398-4f6b-b541-af380d90952e'),
(27, 7, 8, 24, 0, 1, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '531c2de2-8b4a-4c44-9146-8404656193a0'),
(28, 7, 8, 25, 0, 2, '2019-08-30 13:25:42', '2019-08-30 13:25:42', 'bd968d2b-47eb-428d-943e-8af9a42775ed'),
(29, 7, 8, 26, 0, 3, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '76e640c3-0d0d-4f72-bf16-298b510ac0aa'),
(30, 3, 9, 8, 0, 1, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '1f90e411-1625-4a88-86fd-327f33152cdd'),
(31, 3, 9, 9, 0, 2, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '21874217-c9b0-4b1f-848e-6f035b531385'),
(32, 4, 10, 10, 0, 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '9217e160-1d09-487c-80c2-a601d1e2eb8c'),
(33, 4, 10, 11, 0, 2, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '28ed67e4-7ae6-41f8-893c-ccaeb2b82cfc'),
(34, 2, 11, 7, 0, 5, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'edc86142-7e90-4566-8301-e2d9d90e90a5'),
(35, 2, 11, 3, 0, 4, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'faddeea7-12ff-413c-8096-aca222456938'),
(36, 2, 11, 4, 0, 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'f6486218-91c6-4a7c-a4fd-8596032cabc8'),
(37, 2, 11, 12, 0, 6, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '82668c4a-5c36-4c5c-9f19-1e56b410f4ef'),
(38, 2, 11, 5, 0, 3, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'f49bb8b5-04fb-48cc-b83b-4a8340ac4a0b'),
(39, 2, 11, 13, 0, 7, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '4a66e9d2-40cd-4c39-809e-ea5ae44434bc'),
(40, 2, 11, 6, 0, 2, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'e6b41d9b-6fb1-48d4-875d-e91a3003ae71'),
(41, 5, 12, 14, 0, 2, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '5509a65e-697c-4f32-b97b-0baffb0aef67'),
(42, 5, 12, 15, 0, 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '6f6b0065-e5e0-4b24-81d8-4a56ea03ec97'),
(43, 8, 13, 27, 0, 2, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'a45db8ad-95d6-4252-a1a8-8563234d7d6e'),
(44, 8, 13, 28, 0, 5, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '177f5e51-60fc-4ca6-8e01-274000014c5f'),
(45, 8, 13, 29, 0, 3, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '6cf483cb-4247-47d4-8344-df875f8ea5dd'),
(46, 8, 13, 30, 0, 4, '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'c1f295e9-9023-422d-ba3a-4addb2aac101'),
(47, 8, 13, 31, 0, 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '0538b4e7-5e4f-4dcd-a853-263d84131526');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouts`
--

CREATE TABLE `fieldlayouts` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouts`
--

INSERT INTO `fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'craft\\elements\\Entry', '2019-08-30 10:58:35', '2019-08-30 10:58:35', NULL, 'f14418e3-58c3-40bc-9503-8fd6496a1780'),
(2, 'craft\\elements\\MatrixBlock', '2019-08-30 11:01:11', '2019-08-30 11:01:11', NULL, '91b9d666-de0e-4cb2-8671-fc8c370b2bab'),
(3, 'verbb\\supertable\\elements\\SuperTableBlockElement', '2019-08-30 11:04:12', '2019-08-30 11:04:12', NULL, 'c825e4f1-0a14-4ab1-9801-75fc516acff4'),
(4, 'verbb\\supertable\\elements\\SuperTableBlockElement', '2019-08-30 11:04:12', '2019-08-30 11:04:12', NULL, 'b0478e76-3bb0-43c5-a7ec-2ba146b94b66'),
(5, 'craft\\elements\\MatrixBlock', '2019-08-30 11:04:13', '2019-08-30 11:04:13', NULL, 'a717f386-d20c-4d81-9fae-2d87d134325f'),
(6, 'craft\\elements\\GlobalSet', '2019-08-30 11:06:33', '2019-08-30 11:06:33', NULL, '4661905c-0615-4f01-891f-4a7d42b66b67'),
(7, 'craft\\elements\\MatrixBlock', '2019-08-30 13:25:42', '2019-08-30 13:25:42', NULL, '16a9a830-b2ac-4354-b671-aed82796e148'),
(8, 'craft\\elements\\MatrixBlock', '2019-08-30 13:25:43', '2019-08-30 13:25:43', NULL, '45541925-3c56-4278-b2b1-22bae99c00cd');

-- --------------------------------------------------------

--
-- Table structure for table `fieldlayouttabs`
--

CREATE TABLE `fieldlayouttabs` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fieldlayouttabs`
--

INSERT INTO `fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'Tab 1', 1, '2019-08-30 10:58:35', '2019-08-30 10:58:35', 'd6d4737d-f56b-4900-8ac5-ccbdfd9a2532'),
(7, 6, 'Tab 1', 1, '2019-08-30 11:06:33', '2019-08-30 11:06:33', '04b40bd5-03bd-4976-9b0f-bb335bd8612a'),
(8, 7, 'Content', 1, '2019-08-30 13:25:42', '2019-08-30 13:25:42', 'e24d3a43-61a4-402a-9950-94e22945dd74'),
(9, 3, 'Content', 1, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '0328ae2c-6042-4025-9453-a9b1eb6342c1'),
(10, 4, 'Content', 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '9c45184d-f926-4b1d-8141-351d77bc90aa'),
(11, 2, 'Content', 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '9508de20-2b20-451d-b3aa-bc58ca67f66a'),
(12, 5, 'Content', 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '3308bbc5-7a67-4d59-8538-48d872cf2a76'),
(13, 8, 'Content', 1, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '7427cee3-eeba-4237-b76d-514cf93eb091');

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 2, 'Header Image', 'headerImage', 'global', '', 1, 'site', NULL, 'craft\\fields\\Assets', '{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}', '2019-08-30 10:58:24', '2019-08-30 10:58:24', '46f3d757-565b-470e-a3e8-00a836a377c2'),
(2, 1, 'Default Matrix', 'defaultMatrix', 'global', '', 1, 'site', NULL, 'craft\\fields\\Matrix', '{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_defaultmatrix}}\",\"propagationMethod\":\"all\"}', '2019-08-30 11:01:11', '2019-08-30 13:25:42', '3a5879f9-ad42-49ef-b4d0-180562629406'),
(3, NULL, 'Left Column', 'leftColumn', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'site', NULL, 'verbb\\supertable\\fields\\SuperTableField', '{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_1_leftcolumn}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"10\":{\"width\":\"\"},\"11\":{\"width\":\"\"}},\"fieldLayout\":\"table\",\"selectionLabel\":\"\"}', '2019-08-30 11:01:11', '2019-08-30 13:25:42', '2440431b-e069-4b1a-bf1a-317805cdf454'),
(4, NULL, 'Sub Header', 'subHeader', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:01:11', '2019-08-30 13:25:43', '253d97fa-3497-4928-bacb-3e1168f121e2'),
(5, NULL, 'Sub Text', 'subText', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:01:11', '2019-08-30 13:25:43', '5ea935c1-ea3c-4136-91c5-5000c30ec28f'),
(6, NULL, 'Header', 'header', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:01:11', '2019-08-30 13:25:43', 'ab2985c0-04b6-4d0e-adef-1f3228a91662'),
(7, NULL, 'Right Column', 'rightColumn', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'site', NULL, 'verbb\\supertable\\fields\\SuperTableField', '{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_1_rightcolumn}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"8\":{\"width\":\"\"},\"9\":{\"width\":\"\"}},\"fieldLayout\":\"table\",\"selectionLabel\":\"\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:42', '14730285-714e-41e0-8b9f-2587195089dd'),
(8, NULL, 'Heading', 'heading', 'superTableBlockType:22197c31-fb45-4414-9a54-72f379005626', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:42', '47828441-12b1-42e4-9038-98212489a224'),
(9, NULL, 'Text', 'text', 'superTableBlockType:22197c31-fb45-4414-9a54-72f379005626', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:42', 'cde4d12e-3e8d-4392-83c8-00cd70cef3a0'),
(10, NULL, 'Header', 'header', 'superTableBlockType:b2e54d27-83ca-4ab4-ad99-9c5a073410c5', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:42', '1713702d-86a2-469d-b606-3ad1d7318abc'),
(11, NULL, 'Text', 'text', 'superTableBlockType:b2e54d27-83ca-4ab4-ad99-9c5a073410c5', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:42', 'd5b7abf1-d1c7-415c-b0bd-425301c4c22b'),
(12, NULL, 'Top Padding', 'topPadding', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:43', '49be5479-7f6d-4075-aca2-424fad894cb5'),
(13, NULL, 'Bottom Padding', 'bottomPadding', 'matrixBlockType:0831c150-5b5a-4995-839d-4ee88805a628', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 11:04:12', '2019-08-30 13:25:43', '6581fd59-5108-4e70-b018-be965d865ec4'),
(14, NULL, 'Bottom Padding', 'bottomPadding', 'matrixBlockType:dc0ef036-ad22-4ff2-9a1f-e180fcf8d611', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 11:04:13', '2019-08-30 13:25:43', '42a884e6-5fad-4da1-b764-b576d363d01f'),
(15, NULL, 'Top Padding', 'topPadding', 'matrixBlockType:dc0ef036-ad22-4ff2-9a1f-e180fcf8d611', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 11:04:13', '2019-08-30 13:25:43', 'ffe151d4-e082-4586-9f43-2c8a9076c93e'),
(16, 3, 'Address one', 'addressOne', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:14', '2019-08-30 11:05:14', 'a37810d1-5df8-4722-aec5-214bd364e5c0'),
(17, 3, 'Address Two', 'addressTwo', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:20', '2019-08-30 11:05:20', 'd562400a-a424-40b9-aec5-bba43af645b4'),
(18, 3, 'Email', 'email', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:25', '2019-08-30 11:05:25', '54ce5590-1a02-44ab-9cf9-5f0d986bd331'),
(19, 3, 'Facebook', 'facebook', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:30', '2019-08-30 11:05:30', '4990d6e8-9921-483a-b310-d812009d18fc'),
(20, 3, 'Instagram', 'instagram', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:35', '2019-08-30 11:05:35', '88d38636-62af-4539-a60a-682fc38d51bb'),
(21, 3, 'Open Hours', 'openHours', 'global', '', 1, 'none', NULL, 'craft\\redactor\\Field', '{\"redactorConfig\":\"\",\"purifierConfig\":\"\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}', '2019-08-30 11:05:40', '2019-08-30 11:05:56', '6489a53f-b2f0-4688-bb1f-7145ff3265d6'),
(22, 3, 'Telephone', 'telephone', 'global', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 11:05:47', '2019-08-30 11:05:47', '312f3793-cbf5-47b8-b916-51f5c5f09f30'),
(23, 3, 'Logo', 'logo', 'global', '', 1, 'site', NULL, 'craft\\fields\\Assets', '{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}', '2019-08-30 11:06:05', '2019-08-30 11:06:47', '08a80f44-98af-40d5-87c6-cc9c81448d0e'),
(24, NULL, 'Text', 'text', 'matrixBlockType:1d9cf37e-5370-450c-ab96-a7276b025ed5', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 13:25:42', '2019-08-30 13:25:42', '1b2d7929-86f1-4dda-a3b5-8c04cb71c2ee'),
(25, NULL, 'Include SVG ', 'includeSvg', 'matrixBlockType:1d9cf37e-5370-450c-ab96-a7276b025ed5', 'If you want to include SVG with moving text around it', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 13:25:42', '2019-08-30 13:25:42', '645e1d7c-4863-4e55-8524-5650c065bf06'),
(26, NULL, 'SVG', 'svg', 'matrixBlockType:1d9cf37e-5370-450c-ab96-a7276b025ed5', '', 1, 'site', NULL, 'craft\\fields\\Assets', '{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"}', '2019-08-30 13:25:42', '2019-08-30 13:25:42', 'fc635364-236a-4f8d-a1a7-c8d986f277f9'),
(27, NULL, 'Header', 'header', 'matrixBlockType:5b3163e5-4ff3-42b0-9fee-0d8227bba668', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 13:25:43', '2019-08-30 13:25:43', '3b7155cf-629b-4aa1-8e14-d680e7ef5545'),
(28, NULL, 'Bottom Padding', 'bottomPadding', 'matrixBlockType:5b3163e5-4ff3-42b0-9fee-0d8227bba668', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 13:25:43', '2019-08-30 13:25:43', '7e4d3133-cd62-46bd-8b59-b715866bb557'),
(29, NULL, 'Sub Text', 'subText', 'matrixBlockType:5b3163e5-4ff3-42b0-9fee-0d8227bba668', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 13:25:43', '2019-08-30 13:25:43', '93c0a7c9-97d7-43af-8f6b-72433bf2acb3'),
(30, NULL, 'Top Padding', 'topPadding', 'matrixBlockType:5b3163e5-4ff3-42b0-9fee-0d8227bba668', '', 1, 'none', NULL, 'craft\\fields\\Lightswitch', '{\"default\":\"\"}', '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'c0444e1d-5d78-4290-a42f-7a34a8253394'),
(31, NULL, 'Sub Header', 'subHeader', 'matrixBlockType:5b3163e5-4ff3-42b0-9fee-0d8227bba668', '', 1, 'none', NULL, 'craft\\fields\\PlainText', '{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}', '2019-08-30 13:25:43', '2019-08-30 13:25:43', 'ccf232a0-5f04-4fd4-be59-2c65ee90efb5');

-- --------------------------------------------------------

--
-- Table structure for table `globalsets`
--

CREATE TABLE `globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `globalsets`
--

INSERT INTO `globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(5, 'Contact', 'contact', 6, '2019-08-30 11:06:33', '2019-08-30 11:06:33', 'ef4b67a4-c0ed-45e8-865a-e972755cd0b1');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `config` mediumtext,
  `configMap` mediumtext,
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `version`, `schemaVersion`, `maintenance`, `config`, `configMap`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, '3.2.8', '3.2.16', 0, '{\"fieldGroups\":{\"340d14f1-feff-4fc1-852b-b499b39e9730\":{\"name\":\"Common\"},\"57851f76-0174-4774-aa86-604b6c08e5df\":{\"name\":\"Header\"},\"50db6c0a-9c1b-49f4-8452-63157480a752\":{\"name\":\"Contact\"}},\"siteGroups\":{\"1887381b-cdf6-402a-a398-b3baef2d9832\":{\"name\":\"mont\"}},\"sites\":{\"cd174531-de72-46da-a552-53a03be950c5\":{\"siteGroup\":\"1887381b-cdf6-402a-a398-b3baef2d9832\",\"name\":\"mont\",\"handle\":\"default\",\"language\":\"en-US\",\"hasUrls\":true,\"baseUrl\":\"@web/\",\"sortOrder\":1,\"primary\":true}},\"email\":{\"fromEmail\":\"michael@ipopdigital.com\",\"fromName\":\"mont\",\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"},\"system\":{\"edition\":\"solo\",\"name\":\"mont\",\"live\":true,\"schemaVersion\":\"3.2.16\",\"timeZone\":\"America/Los_Angeles\"},\"users\":{\"requireEmailVerification\":true,\"allowPublicRegistration\":false,\"defaultGroup\":null,\"photoVolumeUid\":null,\"photoSubpath\":\"\"},\"dateModified\":1567171542,\"volumes\":{\"939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\":{\"name\":\"Images\",\"handle\":\"images\",\"type\":\"craft\\\\volumes\\\\Local\",\"hasUrls\":true,\"url\":\"@web/build/images\",\"settings\":{\"path\":\"@webroot/build/images\"},\"sortOrder\":1}},\"sections\":{\"2cbeef02-17c4-4905-9d18-6f79da8238f4\":{\"name\":\"Home\",\"handle\":\"home\",\"type\":\"single\",\"enableVersioning\":true,\"propagationMethod\":\"all\",\"siteSettings\":{\"cd174531-de72-46da-a552-53a03be950c5\":{\"enabledByDefault\":true,\"hasUrls\":true,\"uriFormat\":\"__home__\",\"template\":\"index.html\"}},\"entryTypes\":{\"2457c100-8550-4bc6-8888-a82f8977856f\":{\"name\":\"Home\",\"handle\":\"home\",\"hasTitleField\":false,\"titleLabel\":\"\",\"titleFormat\":\"{section.name|raw}\",\"sortOrder\":1,\"fieldLayouts\":{\"f14418e3-58c3-40bc-9503-8fd6496a1780\":{\"tabs\":[{\"name\":\"Tab 1\",\"sortOrder\":1,\"fields\":{\"46f3d757-565b-470e-a3e8-00a836a377c2\":{\"required\":false,\"sortOrder\":1}}}]}}}}}},\"fields\":{\"46f3d757-565b-470e-a3e8-00a836a377c2\":{\"name\":\"Header Image\",\"handle\":\"headerImage\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"57851f76-0174-4774-aa86-604b6c08e5df\"},\"3a5879f9-ad42-49ef-b4d0-180562629406\":{\"name\":\"Default Matrix\",\"handle\":\"defaultMatrix\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Matrix\",\"settings\":{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_defaultmatrix}}\",\"propagationMethod\":\"all\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"340d14f1-feff-4fc1-852b-b499b39e9730\"},\"a37810d1-5df8-4722-aec5-214bd364e5c0\":{\"name\":\"Address one\",\"handle\":\"addressOne\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"d562400a-a424-40b9-aec5-bba43af645b4\":{\"name\":\"Address Two\",\"handle\":\"addressTwo\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"54ce5590-1a02-44ab-9cf9-5f0d986bd331\":{\"name\":\"Email\",\"handle\":\"email\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"4990d6e8-9921-483a-b310-d812009d18fc\":{\"name\":\"Facebook\",\"handle\":\"facebook\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"88d38636-62af-4539-a60a-682fc38d51bb\":{\"name\":\"Instagram\",\"handle\":\"instagram\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"6489a53f-b2f0-4688-bb1f-7145ff3265d6\":{\"name\":\"Open Hours\",\"handle\":\"openHours\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\redactor\\\\Field\",\"settings\":{\"redactorConfig\":\"\",\"purifierConfig\":\"\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"312f3793-cbf5-47b8-b916-51f5c5f09f30\":{\"name\":\"Telephone\",\"handle\":\"telephone\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"},\"08a80f44-98af-40d5-87c6-cc9c81448d0e\":{\"name\":\"Logo\",\"handle\":\"logo\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":\"50db6c0a-9c1b-49f4-8452-63157480a752\"}},\"matrixBlockTypes\":{\"0831c150-5b5a-4995-839d-4ee88805a628\":{\"field\":\"3a5879f9-ad42-49ef-b4d0-180562629406\",\"name\":\"Two Col Text\",\"handle\":\"twoColText\",\"sortOrder\":2,\"fields\":{\"14730285-714e-41e0-8b9f-2587195089dd\":{\"name\":\"Right Column\",\"handle\":\"rightColumn\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_1_rightcolumn}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"8\":{\"width\":\"\"},\"9\":{\"width\":\"\"}},\"fieldLayout\":\"table\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"2440431b-e069-4b1a-bf1a-317805cdf454\":{\"name\":\"Left Column\",\"handle\":\"leftColumn\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\",\"settings\":{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_1_leftcolumn}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"10\":{\"width\":\"\"},\"11\":{\"width\":\"\"}},\"fieldLayout\":\"table\",\"selectionLabel\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null},\"253d97fa-3497-4928-bacb-3e1168f121e2\":{\"name\":\"Sub Header\",\"handle\":\"subHeader\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"49be5479-7f6d-4075-aca2-424fad894cb5\":{\"name\":\"Top Padding\",\"handle\":\"topPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null},\"5ea935c1-ea3c-4136-91c5-5000c30ec28f\":{\"name\":\"Sub Text\",\"handle\":\"subText\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"6581fd59-5108-4e70-b018-be965d865ec4\":{\"name\":\"Bottom Padding\",\"handle\":\"bottomPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null},\"ab2985c0-04b6-4d0e-adef-1f3228a91662\":{\"name\":\"Header\",\"handle\":\"header\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"91b9d666-de0e-4cb2-8671-fc8c370b2bab\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"14730285-714e-41e0-8b9f-2587195089dd\":{\"required\":false,\"sortOrder\":5},\"2440431b-e069-4b1a-bf1a-317805cdf454\":{\"required\":false,\"sortOrder\":4},\"253d97fa-3497-4928-bacb-3e1168f121e2\":{\"required\":false,\"sortOrder\":1},\"49be5479-7f6d-4075-aca2-424fad894cb5\":{\"required\":false,\"sortOrder\":6},\"5ea935c1-ea3c-4136-91c5-5000c30ec28f\":{\"required\":false,\"sortOrder\":3},\"6581fd59-5108-4e70-b018-be965d865ec4\":{\"required\":false,\"sortOrder\":7},\"ab2985c0-04b6-4d0e-adef-1f3228a91662\":{\"required\":false,\"sortOrder\":2}}}]}}},\"dc0ef036-ad22-4ff2-9a1f-e180fcf8d611\":{\"field\":\"3a5879f9-ad42-49ef-b4d0-180562629406\",\"name\":\"Include Learn Achive Motivate\",\"handle\":\"includeLearnAchiveMotivate\",\"sortOrder\":3,\"fields\":{\"42a884e6-5fad-4da1-b764-b576d363d01f\":{\"name\":\"Bottom Padding\",\"handle\":\"bottomPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null},\"ffe151d4-e082-4586-9f43-2c8a9076c93e\":{\"name\":\"Top Padding\",\"handle\":\"topPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null}},\"fieldLayouts\":{\"a717f386-d20c-4d81-9fae-2d87d134325f\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"42a884e6-5fad-4da1-b764-b576d363d01f\":{\"required\":false,\"sortOrder\":2},\"ffe151d4-e082-4586-9f43-2c8a9076c93e\":{\"required\":false,\"sortOrder\":1}}}]}}},\"1d9cf37e-5370-450c-ab96-a7276b025ed5\":{\"field\":\"3a5879f9-ad42-49ef-b4d0-180562629406\",\"name\":\"One Col Text\",\"handle\":\"oneColText\",\"sortOrder\":1,\"fields\":{\"1b2d7929-86f1-4dda-a3b5-8c04cb71c2ee\":{\"name\":\"Text\",\"handle\":\"text\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"645e1d7c-4863-4e55-8524-5650c065bf06\":{\"name\":\"Include SVG \",\"handle\":\"includeSvg\",\"instructions\":\"If you want to include SVG with moving text around it\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"fc635364-236a-4f8d-a1a7-c8d986f277f9\":{\"name\":\"SVG\",\"handle\":\"svg\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"site\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Assets\",\"settings\":{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false,\"validateRelatedElements\":\"\"},\"contentColumnType\":\"string\",\"fieldGroup\":null}},\"fieldLayouts\":{\"16a9a830-b2ac-4354-b671-aed82796e148\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"1b2d7929-86f1-4dda-a3b5-8c04cb71c2ee\":{\"required\":false,\"sortOrder\":1},\"645e1d7c-4863-4e55-8524-5650c065bf06\":{\"required\":false,\"sortOrder\":2},\"fc635364-236a-4f8d-a1a7-c8d986f277f9\":{\"required\":false,\"sortOrder\":3}}}]}}},\"5b3163e5-4ff3-42b0-9fee-0d8227bba668\":{\"field\":\"3a5879f9-ad42-49ef-b4d0-180562629406\",\"name\":\"Show Services\",\"handle\":\"showServices\",\"sortOrder\":4,\"fields\":{\"3b7155cf-629b-4aa1-8e14-d680e7ef5545\":{\"name\":\"Header\",\"handle\":\"header\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"7e4d3133-cd62-46bd-8b59-b715866bb557\":{\"name\":\"Bottom Padding\",\"handle\":\"bottomPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null},\"93c0a7c9-97d7-43af-8f6b-72433bf2acb3\":{\"name\":\"Sub Text\",\"handle\":\"subText\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"c0444e1d-5d78-4290-a42f-7a34a8253394\":{\"name\":\"Top Padding\",\"handle\":\"topPadding\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\Lightswitch\",\"settings\":{\"default\":\"\"},\"contentColumnType\":\"boolean\",\"fieldGroup\":null},\"ccf232a0-5f04-4fd4-be59-2c65ee90efb5\":{\"name\":\"Sub Header\",\"handle\":\"subHeader\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"45541925-3c56-4278-b2b1-22bae99c00cd\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"3b7155cf-629b-4aa1-8e14-d680e7ef5545\":{\"required\":false,\"sortOrder\":2},\"7e4d3133-cd62-46bd-8b59-b715866bb557\":{\"required\":false,\"sortOrder\":5},\"93c0a7c9-97d7-43af-8f6b-72433bf2acb3\":{\"required\":false,\"sortOrder\":3},\"c0444e1d-5d78-4290-a42f-7a34a8253394\":{\"required\":false,\"sortOrder\":4},\"ccf232a0-5f04-4fd4-be59-2c65ee90efb5\":{\"required\":false,\"sortOrder\":1}}}]}}}},\"plugins\":{\"super-table\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.0.10\"},\"redactor\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.2.2\"},\"typedlinkfield\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"}},\"superTableBlockTypes\":{\"22197c31-fb45-4414-9a54-72f379005626\":{\"field\":\"14730285-714e-41e0-8b9f-2587195089dd\",\"fields\":{\"47828441-12b1-42e4-9038-98212489a224\":{\"name\":\"Heading\",\"handle\":\"heading\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"cde4d12e-3e8d-4392-83c8-00cd70cef3a0\":{\"name\":\"Text\",\"handle\":\"text\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"c825e4f1-0a14-4ab1-9801-75fc516acff4\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"47828441-12b1-42e4-9038-98212489a224\":{\"required\":false,\"sortOrder\":1},\"cde4d12e-3e8d-4392-83c8-00cd70cef3a0\":{\"required\":false,\"sortOrder\":2}}}]}}},\"b2e54d27-83ca-4ab4-ad99-9c5a073410c5\":{\"field\":\"2440431b-e069-4b1a-bf1a-317805cdf454\",\"fields\":{\"1713702d-86a2-469d-b606-3ad1d7318abc\":{\"name\":\"Header\",\"handle\":\"header\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null},\"d5b7abf1-d1c7-415c-b0bd-425301c4c22b\":{\"name\":\"Text\",\"handle\":\"text\",\"instructions\":\"\",\"searchable\":true,\"translationMethod\":\"none\",\"translationKeyFormat\":null,\"type\":\"craft\\\\fields\\\\PlainText\",\"settings\":{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"},\"contentColumnType\":\"text\",\"fieldGroup\":null}},\"fieldLayouts\":{\"b0478e76-3bb0-43c5-a7ec-2ba146b94b66\":{\"tabs\":[{\"name\":\"Content\",\"sortOrder\":1,\"fields\":{\"1713702d-86a2-469d-b606-3ad1d7318abc\":{\"required\":false,\"sortOrder\":1},\"d5b7abf1-d1c7-415c-b0bd-425301c4c22b\":{\"required\":false,\"sortOrder\":2}}}]}}}},\"globalSets\":{\"ef4b67a4-c0ed-45e8-865a-e972755cd0b1\":{\"name\":\"Contact\",\"handle\":\"contact\",\"fieldLayouts\":{\"4661905c-0615-4f01-891f-4a7d42b66b67\":{\"tabs\":[{\"name\":\"Tab 1\",\"sortOrder\":1,\"fields\":{\"08a80f44-98af-40d5-87c6-cc9c81448d0e\":{\"required\":false,\"sortOrder\":6},\"312f3793-cbf5-47b8-b916-51f5c5f09f30\":{\"required\":false,\"sortOrder\":8},\"4990d6e8-9921-483a-b310-d812009d18fc\":{\"required\":false,\"sortOrder\":4},\"54ce5590-1a02-44ab-9cf9-5f0d986bd331\":{\"required\":false,\"sortOrder\":3},\"6489a53f-b2f0-4688-bb1f-7145ff3265d6\":{\"required\":false,\"sortOrder\":7},\"88d38636-62af-4539-a60a-682fc38d51bb\":{\"required\":false,\"sortOrder\":5},\"a37810d1-5df8-4722-aec5-214bd364e5c0\":{\"required\":false,\"sortOrder\":1},\"d562400a-a424-40b9-aec5-bba43af645b4\":{\"required\":false,\"sortOrder\":2}}}]}}}}}', '[]', 'L0HjodiMEU7c', '2019-08-30 10:53:11', '2019-08-30 10:53:11', 'ad93889b-5a7a-43c0-a181-2f629abb7a77');

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocks`
--

CREATE TABLE `matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matrixblocktypes`
--

CREATE TABLE `matrixblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matrixblocktypes`
--

INSERT INTO `matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 2, 2, 'Two Col Text', 'twoColText', 2, '2019-08-30 11:01:11', '2019-08-30 13:25:43', '0831c150-5b5a-4995-839d-4ee88805a628'),
(2, 2, 5, 'Include Learn Achive Motivate', 'includeLearnAchiveMotivate', 3, '2019-08-30 11:04:13', '2019-08-30 13:25:43', 'dc0ef036-ad22-4ff2-9a1f-e180fcf8d611'),
(3, 2, 7, 'One Col Text', 'oneColText', 1, '2019-08-30 13:25:42', '2019-08-30 13:25:42', '1d9cf37e-5370-450c-ab96-a7276b025ed5'),
(4, 2, 8, 'Show Services', 'showServices', 4, '2019-08-30 13:25:43', '2019-08-30 13:25:43', '5b3163e5-4ff3-42b0-9fee-0d8227bba668');

-- --------------------------------------------------------

--
-- Table structure for table `matrixcontent_defaultmatrix`
--

CREATE TABLE `matrixcontent_defaultmatrix` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_twoColText_subHeader` text,
  `field_twoColText_subText` text,
  `field_twoColText_header` text,
  `field_twoColText_topPadding` tinyint(1) DEFAULT NULL,
  `field_twoColText_bottomPadding` tinyint(1) DEFAULT NULL,
  `field_includeLearnAchiveMotivate_bottomPadding` tinyint(1) DEFAULT NULL,
  `field_includeLearnAchiveMotivate_topPadding` tinyint(1) DEFAULT NULL,
  `field_oneColText_text` text,
  `field_oneColText_includeSvg` text,
  `field_showServices_header` text,
  `field_showServices_bottomPadding` tinyint(1) DEFAULT NULL,
  `field_showServices_subText` text,
  `field_showServices_topPadding` tinyint(1) DEFAULT NULL,
  `field_showServices_subHeader` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `pluginId`, `type`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 'app', 'Install', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '6dcf5b1d-7562-43af-82f9-eeb36f4ef833'),
(2, NULL, 'app', 'm150403_183908_migrations_table_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '01a33a87-b071-487c-b5a9-0926f3cafb2f'),
(3, NULL, 'app', 'm150403_184247_plugins_table_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '191abc4b-b5d9-4d0e-aa2b-4c24be9cf88e'),
(4, NULL, 'app', 'm150403_184533_field_version', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1b312cd5-1bc4-48cf-a70c-19681cc5002c'),
(5, NULL, 'app', 'm150403_184729_type_columns', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '519b6b11-7e2e-4fc0-bd8e-d1d8eb4edb03'),
(6, NULL, 'app', 'm150403_185142_volumes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '9b2102f4-e859-4af5-a38f-48f495e06a35'),
(7, NULL, 'app', 'm150428_231346_userpreferences', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'e833f325-b7ba-4dbf-9e21-18a3eacf0ae4'),
(8, NULL, 'app', 'm150519_150900_fieldversion_conversion', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '517cd490-6718-4ee6-9531-47ff19fbe307'),
(9, NULL, 'app', 'm150617_213829_update_email_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '0b87472a-43c8-4ae6-aeed-c43ea28d4097'),
(10, NULL, 'app', 'm150721_124739_templatecachequeries', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd726264a-04a2-48e5-a1dd-ef17bae8db1b'),
(11, NULL, 'app', 'm150724_140822_adjust_quality_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '709ac1ac-74f7-4af7-a3ca-ed2c2aa80259'),
(12, NULL, 'app', 'm150815_133521_last_login_attempt_ip', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b90f0a1e-d706-40ab-aecd-a3d7228bec4e'),
(13, NULL, 'app', 'm151002_095935_volume_cache_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8322a47a-23cd-41e0-8ba9-55777f1904a9'),
(14, NULL, 'app', 'm151005_142750_volume_s3_storage_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'cef3d1c6-a65c-4e02-bd5b-d74b4d0cf14d'),
(15, NULL, 'app', 'm151016_133600_delete_asset_thumbnails', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'c3daf96e-87aa-4f35-993e-92553d82d64d'),
(16, NULL, 'app', 'm151209_000000_move_logo', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ced83c12-f6c0-47fd-accc-eb29acf452f5'),
(17, NULL, 'app', 'm151211_000000_rename_fileId_to_assetId', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '561cd4e9-187d-4ab9-a767-72e8b2f19a4a'),
(18, NULL, 'app', 'm151215_000000_rename_asset_permissions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '86dacaf0-630e-4b92-bf2a-19b86b8d088a'),
(19, NULL, 'app', 'm160707_000001_rename_richtext_assetsource_setting', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'a39309f9-0ab1-4dfb-af80-7577b3229464'),
(20, NULL, 'app', 'm160708_185142_volume_hasUrls_setting', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '288bf676-2aca-4b95-b0e7-3b59ac103c7a'),
(21, NULL, 'app', 'm160714_000000_increase_max_asset_filesize', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'aa4aca16-c286-4d2e-9973-598332a8a490'),
(22, NULL, 'app', 'm160727_194637_column_cleanup', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3a995748-9bac-40e0-a0e2-5a3f1750997a'),
(23, NULL, 'app', 'm160804_110002_userphotos_to_assets', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '98a88097-83d6-41df-a05b-68d37f929c86'),
(24, NULL, 'app', 'm160807_144858_sites', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5a538867-eefd-4642-bc3a-031fdaf8781d'),
(25, NULL, 'app', 'm160829_000000_pending_user_content_cleanup', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5e9f5714-515d-484a-85f2-0395f11d7847'),
(26, NULL, 'app', 'm160830_000000_asset_index_uri_increase', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5a04460e-f939-4365-983d-29fc810c7f63'),
(27, NULL, 'app', 'm160912_230520_require_entry_type_id', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8026b36c-1a67-4b6f-b5ed-fbcc39f5072a'),
(28, NULL, 'app', 'm160913_134730_require_matrix_block_type_id', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5d3f75ff-e51a-4b4b-b791-b8549e3741ae'),
(29, NULL, 'app', 'm160920_174553_matrixblocks_owner_site_id_nullable', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '729c1b7b-fbea-4116-9f2d-8177cfad4a42'),
(30, NULL, 'app', 'm160920_231045_usergroup_handle_title_unique', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'c3156e52-567c-44ca-9371-775fd0c06ff9'),
(31, NULL, 'app', 'm160925_113941_route_uri_parts', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '0763fc5d-8ac3-4537-b710-ddb8833fa584'),
(32, NULL, 'app', 'm161006_205918_schemaVersion_not_null', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1184f7d9-e8eb-4ab5-a48b-408f10c7c5b8'),
(33, NULL, 'app', 'm161007_130653_update_email_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '65d5802c-412f-4070-be43-beff8489f805'),
(34, NULL, 'app', 'm161013_175052_newParentId', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1d77e4e1-8ed2-4036-ab1f-56e781e7bad2'),
(35, NULL, 'app', 'm161021_102916_fix_recent_entries_widgets', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'cc081ab9-bdb6-406a-9fe0-51ec8f1aa42a'),
(36, NULL, 'app', 'm161021_182140_rename_get_help_widget', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'a96a3bb8-ebf6-474e-8e3c-fd7791b6646e'),
(37, NULL, 'app', 'm161025_000000_fix_char_columns', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '95626330-b9eb-461c-977f-908524ff3d9a'),
(38, NULL, 'app', 'm161029_124145_email_message_languages', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'de5225db-5a1a-46c5-8dc1-151ec03c29e5'),
(39, NULL, 'app', 'm161108_000000_new_version_format', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'f1c32d6b-f063-43ad-bd2d-d8829788379a'),
(40, NULL, 'app', 'm161109_000000_index_shuffle', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '93d21d01-5999-418f-947c-628e807a41ce'),
(41, NULL, 'app', 'm161122_185500_no_craft_app', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '59191bc5-3abe-4e68-80d2-2b691f4b95c5'),
(42, NULL, 'app', 'm161125_150752_clear_urlmanager_cache', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '6b68d304-6423-4d5c-9e6d-c23591d61ec3'),
(43, NULL, 'app', 'm161220_000000_volumes_hasurl_notnull', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd382fbc0-bf00-4119-ae39-607f3086fc13'),
(44, NULL, 'app', 'm170114_161144_udates_permission', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '0dd6bc7d-6b6a-4f6f-8980-965e899d37a3'),
(45, NULL, 'app', 'm170120_000000_schema_cleanup', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b94d7aa8-bbe8-4bab-9860-d68c175354f3'),
(46, NULL, 'app', 'm170126_000000_assets_focal_point', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3e1a12a9-4a95-4959-b76a-3d2b76132078'),
(47, NULL, 'app', 'm170206_142126_system_name', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '69f5864c-8c1f-40f3-812e-c216aa51a715'),
(48, NULL, 'app', 'm170217_044740_category_branch_limits', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5887c698-cedd-4327-800d-82c98c0462d6'),
(49, NULL, 'app', 'm170217_120224_asset_indexing_columns', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '35f6bfc9-44a1-4d60-a8b0-10da6641043a'),
(50, NULL, 'app', 'm170223_224012_plain_text_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'c56f8103-fba2-468e-a152-e3b363be4ced'),
(51, NULL, 'app', 'm170227_120814_focal_point_percentage', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ee15ef91-ae76-4e43-9bf7-45c831e5f55f'),
(52, NULL, 'app', 'm170228_171113_system_messages', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'caa59fec-2933-4bba-9e54-88f934aa9a93'),
(53, NULL, 'app', 'm170303_140500_asset_field_source_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b46507e8-eea0-4bb5-b0e8-dcd807ea00e5'),
(54, NULL, 'app', 'm170306_150500_asset_temporary_uploads', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'f990221a-34f2-4a20-a584-d2474886508c'),
(55, NULL, 'app', 'm170523_190652_element_field_layout_ids', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'e4e8a8d6-cf7b-4e6b-9ccc-b20f5f646751'),
(56, NULL, 'app', 'm170612_000000_route_index_shuffle', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '55087cd3-bdcf-4264-b99b-5ca5fee579e9'),
(57, NULL, 'app', 'm170621_195237_format_plugin_handles', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '092fa686-824b-4680-935e-f04651d53ebd'),
(58, NULL, 'app', 'm170630_161027_deprecation_line_nullable', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '30e0d7f7-33d4-4957-9105-e0c6098719fc'),
(59, NULL, 'app', 'm170630_161028_deprecation_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'fd320a7b-f990-48e8-99e0-aa6768f1ad38'),
(60, NULL, 'app', 'm170703_181539_plugins_table_tweaks', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '61b47641-7794-49df-81b4-25893f8175ab'),
(61, NULL, 'app', 'm170704_134916_sites_tables', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1244bfdc-3388-4442-9cbf-7d679b4d59b0'),
(62, NULL, 'app', 'm170706_183216_rename_sequences', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '092bc88a-55a5-4fc4-9f03-c9d06aa9d69b'),
(63, NULL, 'app', 'm170707_094758_delete_compiled_traits', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'e05cb384-20c7-4ed4-93e9-ac340d2f8253'),
(64, NULL, 'app', 'm170731_190138_drop_asset_packagist', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'edb27a2c-2678-47b0-9de9-06b8e82d4793'),
(65, NULL, 'app', 'm170810_201318_create_queue_table', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '4844923b-6c86-4788-ba81-eb95028a101c'),
(66, NULL, 'app', 'm170816_133741_delete_compiled_behaviors', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '0448fef7-fb6c-4d3d-9f02-e77aa5b186fc'),
(67, NULL, 'app', 'm170903_192801_longblob_for_queue_jobs', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '23bed81d-4631-4edd-b357-3d664cf0106a'),
(68, NULL, 'app', 'm170914_204621_asset_cache_shuffle', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ce98fc0b-78e5-499c-83f8-31e078be5427'),
(69, NULL, 'app', 'm171011_214115_site_groups', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8b7c9361-09b7-483a-abab-4ac7b48285d0'),
(70, NULL, 'app', 'm171012_151440_primary_site', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8fd8516b-8a17-44f1-b6d7-2ba88bde1943'),
(71, NULL, 'app', 'm171013_142500_transform_interlace', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'f1ed86bd-8b3e-4ea7-8836-28138bda47b8'),
(72, NULL, 'app', 'm171016_092553_drop_position_select', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'c61b6c5c-1759-49f3-b2dd-6bf95d73d100'),
(73, NULL, 'app', 'm171016_221244_less_strict_translation_method', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '9f1a07b3-fcf7-40bc-9788-6a124cf9aeaf'),
(74, NULL, 'app', 'm171107_000000_assign_group_permissions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'f06ea40a-9bae-421e-99f2-f836fd025517'),
(75, NULL, 'app', 'm171117_000001_templatecache_index_tune', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1fdd1822-fa32-40db-98f6-f3705938d80d'),
(76, NULL, 'app', 'm171126_105927_disabled_plugins', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd032e9a8-2f21-4b52-b483-ca6fc2955bed'),
(77, NULL, 'app', 'm171130_214407_craftidtokens_table', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'e0c6341a-cf76-4756-a627-16600937b6d2'),
(78, NULL, 'app', 'm171202_004225_update_email_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '7af7fd0b-e53b-4077-bb3e-00d9e50b0f5c'),
(79, NULL, 'app', 'm171204_000001_templatecache_index_tune_deux', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '6dca4ae8-935f-4814-9ba9-14970f47ffa9'),
(80, NULL, 'app', 'm171205_130908_remove_craftidtokens_refreshtoken_column', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3409759b-2444-4a30-b497-8748a13a8e93'),
(81, NULL, 'app', 'm171218_143135_longtext_query_column', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1b936804-234f-437b-a228-aa21f904d2f0'),
(82, NULL, 'app', 'm171231_055546_environment_variables_to_aliases', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '05cab55d-51df-44e1-9063-4b97c519a27d'),
(83, NULL, 'app', 'm180113_153740_drop_users_archived_column', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '25e3361b-e638-4fa6-9d6d-db1c858a4606'),
(84, NULL, 'app', 'm180122_213433_propagate_entries_setting', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ded8a7de-605d-4ee2-ab16-d483c59029bd'),
(85, NULL, 'app', 'm180124_230459_fix_propagate_entries_values', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ff22c2c6-630e-42bc-be59-ea873e906187'),
(86, NULL, 'app', 'm180128_235202_set_tag_slugs', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '61326c1f-55a1-45e9-b043-5a1a6f714961'),
(87, NULL, 'app', 'm180202_185551_fix_focal_points', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '84fb1011-28dc-47df-be6d-4c7ce56386ae'),
(88, NULL, 'app', 'm180217_172123_tiny_ints', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '76cf0281-e902-46d5-b979-66a4e73cf405'),
(89, NULL, 'app', 'm180321_233505_small_ints', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '9fd00a6a-4d96-4134-83a2-a9a7a6ec8b91'),
(90, NULL, 'app', 'm180328_115523_new_license_key_statuses', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '94397bd4-3e9a-447b-aaa5-37db8a0f38ea'),
(91, NULL, 'app', 'm180404_182320_edition_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '0144f83f-3943-4c49-893f-b87e5c7fe355'),
(92, NULL, 'app', 'm180411_102218_fix_db_routes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '69ba1b59-7c73-4bd4-a775-52a42a1e65d0'),
(93, NULL, 'app', 'm180416_205628_resourcepaths_table', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '316e6d21-43b3-44bd-82b8-d800a611e458'),
(94, NULL, 'app', 'm180418_205713_widget_cleanup', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'c438e568-521b-49c0-b226-180e26abd1d7'),
(95, NULL, 'app', 'm180425_203349_searchable_fields', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '35986db4-fc89-41d9-b192-f9a748773c32'),
(96, NULL, 'app', 'm180516_153000_uids_in_field_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '7b092530-e5e2-4ce1-af63-2715c02dbc11'),
(97, NULL, 'app', 'm180517_173000_user_photo_volume_to_uid', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '660af07c-77fd-4e66-aa97-426136743dfd'),
(98, NULL, 'app', 'm180518_173000_permissions_to_uid', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b7c4b1f2-2890-455a-a7b4-752567fed1c8'),
(99, NULL, 'app', 'm180520_173000_matrix_context_to_uids', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '085d9277-bc26-4f6a-9ab7-58f197701702'),
(100, NULL, 'app', 'm180521_173000_initial_yml_and_snapshot', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '334c3022-c6ca-4328-8ae6-82e7c0d11111'),
(101, NULL, 'app', 'm180731_162030_soft_delete_sites', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b424c083-def8-4a2a-8977-7884dbadc28a'),
(102, NULL, 'app', 'm180810_214427_soft_delete_field_layouts', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '085232b8-0a5e-47c3-82a3-3f90b7ce299d'),
(103, NULL, 'app', 'm180810_214439_soft_delete_elements', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'efb9f2a9-20af-4698-a234-7356bc61e6cb'),
(104, NULL, 'app', 'm180824_193422_case_sensitivity_fixes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'eff83812-73e4-4925-a520-9dcc27a20fae'),
(105, NULL, 'app', 'm180901_151639_fix_matrixcontent_tables', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd4dff915-223e-40e6-9500-92a03f424cf6'),
(106, NULL, 'app', 'm180904_112109_permission_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '342fef13-fb4a-4085-b019-1f3c7a0c4cd1'),
(107, NULL, 'app', 'm180910_142030_soft_delete_sitegroups', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'eb15053a-f8a1-4c9f-b5b9-fa00942fe848'),
(108, NULL, 'app', 'm181011_160000_soft_delete_asset_support', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5978b1d5-6a96-4eec-9a99-6e4523d327cf'),
(109, NULL, 'app', 'm181016_183648_set_default_user_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5f95800c-2059-48c8-9bb0-01cc286a2169'),
(110, NULL, 'app', 'm181017_225222_system_config_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd24e290e-5555-44be-b99f-a4458de1971b'),
(111, NULL, 'app', 'm181018_222343_drop_userpermissions_from_config', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '57f02cfa-0fa1-4d5a-9f9a-54ee35df74f7'),
(112, NULL, 'app', 'm181029_130000_add_transforms_routes_to_config', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3cfc44b2-a834-47c4-b2bb-945a9e55b8a6'),
(113, NULL, 'app', 'm181112_203955_sequences_table', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '65fec1cc-530f-4ee0-a1ea-9f042c7e0922'),
(114, NULL, 'app', 'm181121_001712_cleanup_field_configs', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'e1125612-f4e5-4b11-b4de-93dcbba6c230'),
(115, NULL, 'app', 'm181128_193942_fix_project_config', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '939cdba7-de95-46fb-9c7c-040e4a44679b'),
(116, NULL, 'app', 'm181130_143040_fix_schema_version', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ff413bc2-3953-4943-8028-3fce14439010'),
(117, NULL, 'app', 'm181211_143040_fix_entry_type_uids', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '33da908f-1243-47a2-9425-082d4f4dcd31'),
(118, NULL, 'app', 'm181213_102500_config_map_aliases', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '029ff71d-2db3-46c5-b96c-8e0555224a37'),
(119, NULL, 'app', 'm181217_153000_fix_structure_uids', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '65eedcaf-9d8e-4ef0-aac6-6e6a484eb065'),
(120, NULL, 'app', 'm190104_152725_store_licensed_plugin_editions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b1a759d6-3a69-4ae0-afcd-6b87ca063aa1'),
(121, NULL, 'app', 'm190108_110000_cleanup_project_config', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '9bf51785-e352-4678-95b1-9be9c025b814'),
(122, NULL, 'app', 'm190108_113000_asset_field_setting_change', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3f79be09-64c6-494d-aa6b-80dec5cc3712'),
(123, NULL, 'app', 'm190109_172845_fix_colspan', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8d2dfc89-16f9-4838-b4a1-cc86189bf793'),
(124, NULL, 'app', 'm190110_150000_prune_nonexisting_sites', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'b0b7290d-b12d-4919-8266-b456c5a07d8e'),
(125, NULL, 'app', 'm190110_214819_soft_delete_volumes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '45a84a1b-f0eb-440d-8d79-0110fd2eb95a'),
(126, NULL, 'app', 'm190112_124737_fix_user_settings', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '48d32df2-3962-4817-b876-af234e441a50'),
(127, NULL, 'app', 'm190112_131225_fix_field_layouts', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '29b15e32-58f2-4908-97c3-d3a65b0ebf67'),
(128, NULL, 'app', 'm190112_201010_more_soft_deletes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5ae55c31-b37f-4607-9c48-c8dd567598e6'),
(129, NULL, 'app', 'm190114_143000_more_asset_field_setting_changes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd2f5fe18-a2d9-4b1a-951c-0285ac2b69cf'),
(130, NULL, 'app', 'm190121_120000_rich_text_config_setting', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2a077610-1203-4b8d-a64e-f7b51ba82c8e'),
(131, NULL, 'app', 'm190125_191628_fix_email_transport_password', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '7502dfea-eba1-4a58-8881-e56bf5339fe6'),
(132, NULL, 'app', 'm190128_181422_cleanup_volume_folders', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '5d9ca782-29ca-44a8-92ac-771302824e4a'),
(133, NULL, 'app', 'm190205_140000_fix_asset_soft_delete_index', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '907fbaa5-c496-4c26-8a29-df8bafc59532'),
(134, NULL, 'app', 'm190208_140000_reset_project_config_mapping', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '3d8bf3e9-f386-4eac-96bc-adbd09f54bef'),
(135, NULL, 'app', 'm190218_143000_element_index_settings_uid', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '87c656f3-2075-47ff-8f26-dede8b87ebc0'),
(136, NULL, 'app', 'm190312_152740_element_revisions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'd6aaca9e-2f2f-4525-a230-05a5f0658c0e'),
(137, NULL, 'app', 'm190327_235137_propagation_method', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '749253b2-db1f-48ba-824d-4e486468c0aa'),
(138, NULL, 'app', 'm190401_223843_drop_old_indexes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '1e48fe80-c1ab-43dc-ae9d-80962e80810d'),
(139, NULL, 'app', 'm190416_014525_drop_unique_global_indexes', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '509bfb81-ad69-4bff-9251-2874cb3db032'),
(140, NULL, 'app', 'm190417_085010_add_image_editor_permissions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '6cbefe7c-a020-46a1-821b-099552c916cb'),
(141, NULL, 'app', 'm190502_122019_store_default_user_group_uid', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'ab8ca320-173d-48e9-8300-f6d0dd7a1757'),
(142, NULL, 'app', 'm190504_150349_preview_targets', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '75c5de79-5cac-4e2c-a37a-3ad719636e19'),
(143, NULL, 'app', 'm190516_184711_job_progress_label', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '6957ab06-49d7-43e9-a7cb-ceefbd014c11'),
(144, NULL, 'app', 'm190523_190303_optional_revision_creators', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '7478fd22-79e9-4a6d-a6bf-315bf432d4ed'),
(145, NULL, 'app', 'm190529_204501_fix_duplicate_uids', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '8f0b4488-e43e-4793-84f5-91bccfc8d5f2'),
(146, NULL, 'app', 'm190605_223807_unsaved_drafts', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'cf1c7134-355d-4c04-bcbc-046fb8201194'),
(147, NULL, 'app', 'm190607_230042_entry_revision_error_tables', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '4c65bd42-ec9e-411f-a099-e0b35139ffb1'),
(148, NULL, 'app', 'm190608_033429_drop_elements_uid_idx', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '715c66b9-0c9f-4cbc-af46-886b225a47da'),
(149, NULL, 'app', 'm190624_234204_matrix_propagation_method', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'f96ba86b-fc2a-42db-93fc-34749fdc5b3d'),
(150, NULL, 'app', 'm190711_153020_drop_snapshots', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '92ab582a-2b3a-4f62-be78-7b96827f3800'),
(151, NULL, 'app', 'm190712_195914_no_draft_revisions', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'a495e8f7-bbe3-46a3-8923-40c4f21287d3'),
(152, NULL, 'app', 'm190723_140314_fix_preview_targets_column', '2019-08-30 10:53:14', '2019-08-30 10:53:14', '2019-08-30 10:53:14', 'bf695f8d-06e5-486b-9ba9-552c17668351'),
(153, 1, 'plugin', 'Install', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '74af7a5a-d286-4a71-ab9f-4c3179be5a92'),
(154, 1, 'plugin', 'm180210_000000_migrate_content_tables', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', 'bef4f58c-478c-4c2f-bd96-07214028f12b'),
(155, 1, 'plugin', 'm180211_000000_type_columns', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '590e23c6-eb34-4b09-b596-b2e060a6d932'),
(156, 1, 'plugin', 'm180219_000000_sites', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', 'e3654a3f-9c2e-48e7-ba45-34214f0af09d'),
(157, 1, 'plugin', 'm180220_000000_fix_context', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '8a7355c9-789d-4b7b-b795-83ef2a84350b'),
(158, 1, 'plugin', 'm190117_000000_soft_deletes', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '28835112-322f-4c94-b168-1fac3e9a72fa'),
(159, 1, 'plugin', 'm190117_000001_context_to_uids', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '8c812ebd-cc73-4c08-a104-cb14d32ab163'),
(160, 1, 'plugin', 'm190120_000000_fix_supertablecontent_tables', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '09ba4762-b022-41e0-9c0d-b2c3dfe0f010'),
(161, 1, 'plugin', 'm190131_000000_fix_supertable_missing_fields', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '5d49c234-d84d-489e-94b2-ae49a7543008'),
(162, 1, 'plugin', 'm190227_100000_fix_project_config', '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:18', 'a4c91f19-7ad7-41bb-8137-c429582c4f1e'),
(163, 2, 'plugin', 'm180430_204710_remove_old_plugins', '2019-08-30 11:01:22', '2019-08-30 11:01:22', '2019-08-30 11:01:22', '5dff13d2-1847-450b-b7b2-5f5d718ea371'),
(164, 2, 'plugin', 'Install', '2019-08-30 11:01:22', '2019-08-30 11:01:22', '2019-08-30 11:01:22', '5675b52c-da89-43cf-851c-f0af8f9bad1f');

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE `plugins` (
  `id` int(11) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `handle`, `version`, `schemaVersion`, `licenseKeyStatus`, `licensedEdition`, `installDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'super-table', '2.1.16', '2.0.10', 'unknown', NULL, '2019-08-30 11:01:18', '2019-08-30 11:01:18', '2019-08-30 11:01:29', '2cd4426a-7a61-405c-ae1c-b491e4614dad'),
(2, 'redactor', '2.3.2', '2.2.2', 'unknown', NULL, '2019-08-30 11:01:22', '2019-08-30 11:01:22', '2019-08-30 11:01:29', '8ca2bab5-b4ca-49bb-9709-62f48870cbd7'),
(3, 'typedlinkfield', '1.0.17', '1.0.0', 'unknown', NULL, '2019-08-30 11:01:27', '2019-08-30 11:01:27', '2019-08-30 11:01:29', 'e6f7fef0-5f1e-466f-b59a-fb44f4fbfb23');

-- --------------------------------------------------------

--
-- Table structure for table `queue`
--

CREATE TABLE `queue` (
  `id` int(11) NOT NULL,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) UNSIGNED NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 2, NULL, 6, 1, '2019-08-30 13:41:47', '2019-08-30 13:41:47', '16d63599-2375-40eb-9ad6-7cfe696d68e2'),
(2, 1, 7, NULL, 6, 1, '2019-08-30 13:41:47', '2019-08-30 13:41:47', '333caf9a-d280-403f-8cb1-c7f9d24725a1');

-- --------------------------------------------------------

--
-- Table structure for table `resourcepaths`
--

CREATE TABLE `resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `resourcepaths`
--

INSERT INTO `resourcepaths` (`hash`, `path`) VALUES
('1a4c64c1', '@app/web/assets/login/dist'),
('1aa99b73', '@verbb/supertable/resources/dist'),
('1e7271bf', '@app/web/assets/cp/dist'),
('25dfd780', '@lib/jquery-ui'),
('2921784d', '@app/web/assets/generalsettings/dist'),
('3317b89d', '@app/web/assets/craftsupport/dist'),
('33f5632', '@app/web/assets/recententries/dist'),
('3b616631', '@craft/web/assets/plugins/dist'),
('3d85145c', '@app/web/assets/updateswidget/dist'),
('404c5869', '@craft/web/assets/matrixsettings/dist'),
('42d37aa0', '@lib'),
('556910ad', '@lib/element-resize-detector'),
('56a36e6e', '@app/web/assets/matrixsettings/dist'),
('67693a01', '@app/web/assets/feed/dist'),
('6b764c25', '@lib/velocity'),
('6cbb8f71', '@typedlinkfield/resources'),
('75235d71', '@app/web/assets/tablesettings/dist'),
('7751e3cd', '@lib/jquery.payment'),
('77ce345a', '@craft/redactor/assets/field/dist'),
('7af9bb58', '@bower/jquery/dist'),
('7f207905', '@craft/web/assets/editentry/dist'),
('97beb6a4', '@lib/timepicker'),
('9fb472e0', '@app/web/assets/plugins/dist'),
('a22daa5b', '@lib/selectize'),
('a4028f12', '@craft/web/assets/cp/dist'),
('a841eed1', '@lib/xregexp'),
('a94a9da', '@vendor/craftcms/redactor/lib/redactor'),
('cb4bb008', '@lib/garnishjs'),
('da1811bd', '@app/web/assets/fields/dist'),
('dd7f3ff3', '@app/web/assets/dashboard/dist'),
('de0d6436', '@lib/fabric'),
('dec8addb', '@craft/web/assets/fields/dist'),
('ebb2567d', '@app/web/assets/sites/dist'),
('eebb8621', '@lib/fileupload'),
('f1f7bb1d', '@lib/jquery-touch-events'),
('f371b194', '@lib/prismjs'),
('f65387df', '@lib/picturefill'),
('f6da1fae', '@craft/web/assets/tablesettings/dist'),
('f8f6a78e', '@lib/d3');

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `revisions`
--

INSERT INTO `revisions` (`id`, `sourceId`, `creatorId`, `num`, `notes`) VALUES
(1, 2, 1, 1, NULL),
(2, 2, 1, 2, NULL),
(3, 2, 1, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `searchindex`
--

CREATE TABLE `searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `searchindex`
--

INSERT INTO `searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`) VALUES
(1, 'username', 0, 1, ' theboss '),
(1, 'firstname', 0, 1, ''),
(1, 'lastname', 0, 1, ''),
(1, 'fullname', 0, 1, ''),
(1, 'email', 0, 1, ' michael ipopdigital com '),
(1, 'slug', 0, 1, ''),
(2, 'slug', 0, 1, ' home '),
(2, 'title', 0, 1, ' home '),
(2, 'field', 1, 1, ' placeholder '),
(5, 'slug', 0, 1, ''),
(5, 'field', 16, 1, ''),
(5, 'field', 17, 1, ''),
(5, 'field', 18, 1, ''),
(5, 'field', 19, 1, ''),
(5, 'field', 20, 1, ''),
(5, 'field', 23, 1, ''),
(5, 'field', 21, 1, ''),
(5, 'field', 22, 1, ''),
(6, 'filename', 0, 1, ' placeholder png '),
(6, 'extension', 0, 1, ' png '),
(6, 'kind', 0, 1, ' image '),
(6, 'slug', 0, 1, ''),
(6, 'title', 0, 1, ' placeholder ');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagationMethod`, `previewTargets`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'Home', 'home', 'single', 1, 'all', NULL, '2019-08-30 10:57:24', '2019-08-30 10:58:35', NULL, '2cbeef02-17c4-4905-9d18-6f79da8238f4');

-- --------------------------------------------------------

--
-- Table structure for table `sections_sites`
--

CREATE TABLE `sections_sites` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_sites`
--

INSERT INTO `sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 1, 1, '__home__', 'index.html', 1, '2019-08-30 10:57:24', '2019-08-30 10:58:35', 'aacbc100-f819-485b-bb93-6642bf331ff3');

-- --------------------------------------------------------

--
-- Table structure for table `sequences`
--

CREATE TABLE `sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'tnF5P51zn94xaH74MHovR_xlGfMgq4BM-VEWCnZVHOwTnPLO7kUMwiE4tZ-x2OMQK5Gd0krGNBHMyVHCVlELQA7gbwt83FwbUXpm', '2019-08-30 10:53:14', '2019-08-30 10:53:20', 'e702034f-ab1b-4c76-ab18-84f8d38906cb'),
(2, 1, '9vNnCGEQE8TShIWFE8LoSzcR8rz3rAAQx9WL-ByRVJ-myI-0FSsOUaN0wTFCnni3bwJBVAvYwOpUJuCknvocf8ftzhORDXsyTK4q', '2019-08-30 10:53:20', '2019-08-30 10:53:21', '55ac6300-59fa-40bd-b4e0-4fcc396d1c79'),
(3, 1, '_liTaRCtfXtMirEaSsbl0EBsI4IvtMaHHyBXCnZ9L86ze3cf1pPQfMuFNcaLS1V4UZ8NBVfm_CxC_Yc6KqDfgdYDmUBrp7NJxSKz', '2019-08-30 10:53:24', '2019-08-30 10:53:24', '30215d92-c814-46fa-88db-1a67897df728'),
(4, 1, 'klGIMagv9u2_xxYw--MlhazQJTVyYE4o6mxjJge93uPRvmugG-i8IhwfbJ6ZTZuJ9PJ5Ihz-Pvjz8D1Z37wdpBkrTNIA8K0Z7O3q', '2019-08-30 10:55:37', '2019-08-30 11:07:50', 'fec4d3ef-0d7d-4ba4-9d01-a980973a29df'),
(5, 1, 'bKUaHOo9N30autCvq-XKzJ8ZnjNWH5Hlxx5AWV5SBgD9Ug7xGgLKwxAgCy1s-EMgZWukjHY1TYc8inPHstVws615ExW0QtU8Haut', '2019-08-30 11:09:51', '2019-08-30 11:09:51', 'f297e435-36a3-4269-ad4e-380ffecf133a'),
(6, 1, 'Egz6IVIDC1lVJGz9Z3rbVAYjdLgF9w4aHoEsFte1bvoIIEK494IUie1ohR2NHfNnu2doAQSxp_fLXg-4tlpyIVYXntyoCwJhmB9G', '2019-08-30 11:11:53', '2019-08-30 11:11:53', '70dc3e41-d70e-4ddb-ab7d-4442b4a730bd'),
(7, 1, 'U1cE_M5Me1dLQ5FHbJxkCNNLO4aGauu-FnKeaKfrIoYyqxF6RznnBT95_h_GHBpqvwW_yLkwsHJWzO-qCPM6O7lx4SjuyqIALsWb', '2019-08-30 11:12:54', '2019-08-30 11:12:54', '70af61e1-ce5b-4c15-a4e4-49126da38f9f'),
(8, 1, 'pQJmfWMsh5ZwObLuxpXaOAsvOW5-sIH3PWEvQaQFPgw_4qnzMDdkTgmATmJ0_iA4qggL0IC_PTX5ctjj_Oo0SiIPERFuWCvb5Pxw', '2019-08-30 11:14:56', '2019-08-30 11:14:56', 'db5479ca-af72-426f-8d7a-929730ecf928'),
(9, 1, 'yXmGb4Fff98e3ne0v_XiHrGDOorvzQUJITROYK3OKFnNj5AbUbGhvkodHbTh7UJIakc58dXWfOU8FGN4Vj7ouFe1vzFdk_3XAiEz', '2019-08-30 11:19:00', '2019-08-30 11:19:00', '1d85b046-ec5e-4b44-b59d-80585b1a4be8'),
(10, 1, '8tQJwDzEXiEyHGnlCg-0QaGAtKiBi3nF5h0rwkyh_nRkLe_YPPSgCQ0k0QHSLribjCjLwtgI8XT3HJz1NlsdJCGM4sA829lrHaLd', '2019-08-30 11:20:01', '2019-08-30 11:20:01', 'a4544d3c-c314-4611-9e47-180b96e92610'),
(11, 1, 'hyWPkwVz0YTVjqnLpd4u_lNp8UY989kzNV8NIFuDXuMU4tskeQLPY8DFCAUlrCRT9jc4LyDu92nH_kB7-ExgBFpetrqd3csGOgqs', '2019-08-30 11:22:03', '2019-08-30 11:22:03', '5a924e2f-1099-460e-b645-8007a78bbf7e'),
(12, 1, '-eipCWteG6c2FjMbHfBEyMSRpWsWY5knjfmLGo0hn8KkkKugyHajFNaAuTDzr4PLrIVdlGjJLP8uIUyyzjx2h_kY5ZjUh3hpGgl3', '2019-08-30 11:29:10', '2019-08-30 11:29:10', '4aacfdd1-7deb-485f-ab71-976522826e61'),
(13, 1, 'NpoROdz1gSO-aBS9kJlw-1qN0ThJVAogGzI-ZMw5ZTeUwqPDSJeD1pCwPL8fKD8jqAfuIme1KMj6XUQnev3lRw5NOJqfD73uLob5', '2019-08-30 11:33:14', '2019-08-30 11:33:14', '79b32a59-d27a-460d-9473-577f5515d886'),
(14, 1, 'jJQ4hNHApqQqAzrMSYmYJcd1LIqD8AgZqCiGaLFPvqKd1fdOfTGqce3UojVHOu3bk_Z3tkIf0cOpyKdHsCfMfXr1sd2LJxIxGFUG', '2019-08-30 11:34:15', '2019-08-30 11:34:15', '537d94ce-845a-42cb-8a3a-6341965b821c'),
(15, 1, 'BPvwVtlvTcoiNdC5RO4IwiKUb4w2FTned08RItTiAqvaB7IqRxL23GvPC9uv7_u9-LPYzfKS-UDaxCpGVRWn5hK1giN_FKvg9u-K', '2019-08-30 11:37:18', '2019-08-30 11:37:18', '2868082a-1510-4590-9a87-4c7999e5ddf7'),
(16, 1, '7xWRqmPGsnYU85yrZhc1WdmwtBj3k7fc7Uy8FpxM64-Wn63M9pliBSah1Cg9tNnJFaOLMcK0tyVdgeB2q4L3MO5Dvwxad4Jdfg06', '2019-08-30 13:06:09', '2019-08-30 13:06:09', 'ce111b9f-ad24-4f6d-ac2d-9953608eb190'),
(17, 1, '5k9Fe9NSAjFFJR3BIIcw_LeMsrGIhtOBxPVHQiXKjtsGoH3xQQnBRveUBV-8g7iuhgDxqf0hV1Z6W9MJ14rQQ91176N7PPFogXGN', '2019-08-30 13:10:13', '2019-08-30 13:10:13', '85704ae2-2f57-4b6e-a339-83b82f592666'),
(18, 1, 'fP6jV3QA5XiKyWEk0hGJypbKsltP5qbXlwkwboRhXfV7jvVYxlyJdFLvXXPqLR2acgmkhVTTJCeY9L0cfvAqHCFgpNE6A-JFuUlZ', '2019-08-30 13:11:14', '2019-08-30 13:11:14', 'b50694bb-6dd6-468b-a2ac-f6707fb8dc73'),
(19, 1, 'hJilybVxXo9MDWaIPMlvnFvcY4bRf7N4Tgxoq-yuxazn8ggDnQnYnvB3OLfi01JESieDmj1Lf-QazNyWJtG__cJjSZtn-Bt1DQE-', '2019-08-30 13:13:16', '2019-08-30 13:13:16', '0cb49d0b-85f8-4ea8-bd07-4ee0d2c27259'),
(20, 1, '6pWitFLCLaYfdqVqLigzeeICjKilDSFFx57tAl1fFYVMDk-_6pKdU10XJCNKI9TtKoTcWnjGNsyeZ_yftGY01uMloqhR-uHuIxHq', '2019-08-30 13:14:17', '2019-08-30 13:42:59', 'b3d8096d-a14f-49fe-a4aa-5698e2947de5');

-- --------------------------------------------------------

--
-- Table structure for table `shunnedmessages`
--

CREATE TABLE `shunnedmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sitegroups`
--

CREATE TABLE `sitegroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitegroups`
--

INSERT INTO `sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 'mont', '2019-08-30 10:53:11', '2019-08-30 10:53:11', NULL, '1887381b-cdf6-402a-a398-b3baef2d9832');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `groupId`, `primary`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, 1, 1, 'mont', 'default', 'en-US', 1, '@web/', 1, '2019-08-30 10:53:11', '2019-08-30 10:57:00', NULL, 'cd174531-de72-46da-a552-53a03be950c5');

-- --------------------------------------------------------

--
-- Table structure for table `stc_1_leftcolumn`
--

CREATE TABLE `stc_1_leftcolumn` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_header` text,
  `field_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stc_1_rightcolumn`
--

CREATE TABLE `stc_1_rightcolumn` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_heading` text,
  `field_text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structureelements`
--

CREATE TABLE `structureelements` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `structures`
--

CREATE TABLE `structures` (
  `id` int(11) NOT NULL,
  `maxLevels` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supertableblocks`
--

CREATE TABLE `supertableblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supertableblocktypes`
--

CREATE TABLE `supertableblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supertableblocktypes`
--

INSERT INTO `supertableblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 7, 3, '2019-08-30 11:04:12', '2019-08-30 11:04:12', '22197c31-fb45-4414-9a54-72f379005626'),
(2, 3, 4, '2019-08-30 11:04:12', '2019-08-30 11:04:12', 'b2e54d27-83ca-4ab4-ad99-9c5a073410c5');

-- --------------------------------------------------------

--
-- Table structure for table `systemmessages`
--

CREATE TABLE `systemmessages` (
  `id` int(11) NOT NULL,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taggroups`
--

CREATE TABLE `taggroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecacheelements`
--

CREATE TABLE `templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecachequeries`
--

CREATE TABLE `templatecachequeries` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `templatecaches`
--

CREATE TABLE `templatecaches` (
  `id` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) UNSIGNED DEFAULT NULL,
  `usageCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups`
--

CREATE TABLE `usergroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usergroups_users`
--

CREATE TABLE `usergroups_users` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions`
--

CREATE TABLE `userpermissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_usergroups`
--

CREATE TABLE `userpermissions_usergroups` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpermissions_users`
--

CREATE TABLE `userpermissions_users` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `userpreferences`
--

CREATE TABLE `userpreferences` (
  `userId` int(11) NOT NULL,
  `preferences` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userpreferences`
--

INSERT INTO `userpreferences` (`userId`, `preferences`) VALUES
(1, '{\"language\":\"en-US\"}');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 'theboss', NULL, NULL, NULL, 'michael@ipopdigital.com', '$2y$13$kHb3f15/YhU.OCuzSkRlEu.RvOFSEbIBWy3qvRXu907BjBpUFXW3a', 1, 0, 0, 0, '2019-08-30 13:14:17', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 0, '2019-08-30 10:53:13', '2019-08-30 10:53:13', '2019-08-30 13:14:17', '6088e5da-1938-43e0-afb7-086a8d6b3de2');

-- --------------------------------------------------------

--
-- Table structure for table `volumefolders`
--

CREATE TABLE `volumefolders` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumefolders`
--

INSERT INTO `volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, NULL, 1, 'Images', '', '2019-08-30 10:56:21', '2019-08-30 10:56:21', '1bb22228-d920-4ff4-b511-a105e438543f'),
(2, NULL, NULL, 'Temporary source', NULL, '2019-08-30 11:01:33', '2019-08-30 11:01:33', '4df33c1e-b260-4a58-a6e7-92301ea66b68'),
(3, 2, NULL, 'user_1', 'user_1/', '2019-08-30 11:01:33', '2019-08-30 11:01:33', 'b124df85-8223-4dd7-a884-b80aae01f29b');

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE `volumes` (
  `id` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `volumes`
--

INSERT INTO `volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`) VALUES
(1, NULL, 'Images', 'images', 'craft\\volumes\\Local', 1, '@web/build/images', '{\"path\":\"@webroot/build/images\"}', 1, '2019-08-30 10:56:21', '2019-08-30 10:56:21', NULL, '939d48d4-4b68-4d2d-8a71-d8cbb7e2e98d');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
(1, 1, 'craft\\widgets\\RecentEntries', 1, NULL, '{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}', 1, '2019-08-30 10:53:16', '2019-08-30 10:53:16', '062e62b8-a61b-4f56-9ce9-d52f28adc48c'),
(2, 1, 'craft\\widgets\\CraftSupport', 2, NULL, '[]', 1, '2019-08-30 10:53:16', '2019-08-30 10:53:16', 'ed1d625a-526c-4544-a065-e25328bb964f'),
(3, 1, 'craft\\widgets\\Updates', 3, NULL, '[]', 1, '2019-08-30 10:53:16', '2019-08-30 10:53:16', '87ee483d-5f0e-4be8-a00d-5ee51c06a563'),
(4, 1, 'craft\\widgets\\Feed', 4, NULL, '{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}', 1, '2019-08-30 10:53:16', '2019-08-30 10:53:16', '1df67a39-93bb-46ad-aaa7-2fe038a818b0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  ADD KEY `assetindexdata_volumeId_idx` (`volumeId`);

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assets_filename_folderId_idx` (`filename`,`folderId`),
  ADD KEY `assets_folderId_idx` (`folderId`),
  ADD KEY `assets_volumeId_idx` (`volumeId`);

--
-- Indexes for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`);

--
-- Indexes for table `assettransforms`
--
ALTER TABLE `assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assettransforms_name_unq_idx` (`name`),
  ADD UNIQUE KEY `assettransforms_handle_unq_idx` (`handle`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_groupId_idx` (`groupId`),
  ADD KEY `categories_parentId_fk` (`parentId`);

--
-- Indexes for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorygroups_name_idx` (`name`),
  ADD KEY `categorygroups_handle_idx` (`handle`),
  ADD KEY `categorygroups_structureId_idx` (`structureId`),
  ADD KEY `categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `categorygroups_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  ADD KEY `categorygroups_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `content_siteId_idx` (`siteId`),
  ADD KEY `content_title_idx` (`title`);

--
-- Indexes for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craftidtokens_userId_fk` (`userId`);

--
-- Indexes for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`);

--
-- Indexes for table `drafts`
--
ALTER TABLE `drafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `drafts_creatorId_fk` (`creatorId`),
  ADD KEY `drafts_sourceId_fk` (`sourceId`);

--
-- Indexes for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elementindexsettings_type_unq_idx` (`type`);

--
-- Indexes for table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `elements_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `elements_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `elements_type_idx` (`type`),
  ADD KEY `elements_enabled_idx` (`enabled`),
  ADD KEY `elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  ADD KEY `elements_draftId_fk` (`draftId`),
  ADD KEY `elements_revisionId_fk` (`revisionId`);

--
-- Indexes for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `elements_sites_siteId_idx` (`siteId`),
  ADD KEY `elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  ADD KEY `elements_sites_enabled_idx` (`enabled`),
  ADD KEY `elements_sites_uri_siteId_idx` (`uri`,`siteId`);

--
-- Indexes for table `entries`
--
ALTER TABLE `entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entries_postDate_idx` (`postDate`),
  ADD KEY `entries_expiryDate_idx` (`expiryDate`),
  ADD KEY `entries_authorId_idx` (`authorId`),
  ADD KEY `entries_sectionId_idx` (`sectionId`),
  ADD KEY `entries_typeId_idx` (`typeId`),
  ADD KEY `entries_parentId_fk` (`parentId`);

--
-- Indexes for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  ADD KEY `entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  ADD KEY `entrytypes_sectionId_idx` (`sectionId`),
  ADD KEY `entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `entrytypes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldgroups_name_unq_idx` (`name`);

--
-- Indexes for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  ADD KEY `fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayoutfields_tabId_idx` (`tabId`),
  ADD KEY `fieldlayoutfields_fieldId_idx` (`fieldId`);

--
-- Indexes for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `fieldlayouts_type_idx` (`type`);

--
-- Indexes for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  ADD KEY `fieldlayouttabs_layoutId_idx` (`layoutId`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fields_handle_context_unq_idx` (`handle`,`context`),
  ADD KEY `fields_groupId_idx` (`groupId`),
  ADD KEY `fields_context_idx` (`context`);

--
-- Indexes for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `globalsets_name_idx` (`name`),
  ADD KEY `globalsets_handle_idx` (`handle`),
  ADD KEY `globalsets_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matrixblocks_ownerId_idx` (`ownerId`),
  ADD KEY `matrixblocks_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocks_typeId_idx` (`typeId`),
  ADD KEY `matrixblocks_sortOrder_idx` (`sortOrder`);

--
-- Indexes for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  ADD UNIQUE KEY `matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  ADD KEY `matrixblocktypes_fieldId_idx` (`fieldId`),
  ADD KEY `matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `matrixcontent_defaultmatrix`
--
ALTER TABLE `matrixcontent_defaultmatrix`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matrixcontent_defaultmatrix_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `matrixcontent_defaultmatrix_siteId_fk` (`siteId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `migrations_pluginId_idx` (`pluginId`),
  ADD KEY `migrations_type_pluginId_idx` (`type`,`pluginId`);

--
-- Indexes for table `plugins`
--
ALTER TABLE `plugins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plugins_handle_unq_idx` (`handle`);

--
-- Indexes for table `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  ADD KEY `queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  ADD KEY `relations_sourceId_idx` (`sourceId`),
  ADD KEY `relations_targetId_idx` (`targetId`),
  ADD KEY `relations_sourceSiteId_idx` (`sourceSiteId`);

--
-- Indexes for table `resourcepaths`
--
ALTER TABLE `resourcepaths`
  ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  ADD KEY `revisions_creatorId_fk` (`creatorId`);

--
-- Indexes for table `searchindex`
--
ALTER TABLE `searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`);
ALTER TABLE `searchindex` ADD FULLTEXT KEY `searchindex_keywords_idx` (`keywords`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sections_handle_idx` (`handle`),
  ADD KEY `sections_name_idx` (`name`),
  ADD KEY `sections_structureId_idx` (`structureId`),
  ADD KEY `sections_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  ADD KEY `sections_sites_siteId_idx` (`siteId`);

--
-- Indexes for table `sequences`
--
ALTER TABLE `sequences`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_uid_idx` (`uid`),
  ADD KEY `sessions_token_idx` (`token`),
  ADD KEY `sessions_dateUpdated_idx` (`dateUpdated`),
  ADD KEY `sessions_userId_idx` (`userId`);

--
-- Indexes for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shunnedmessages_userId_message_unq_idx` (`userId`,`message`);

--
-- Indexes for table `sitegroups`
--
ALTER TABLE `sitegroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sitegroups_name_idx` (`name`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `sites_handle_idx` (`handle`),
  ADD KEY `sites_sortOrder_idx` (`sortOrder`),
  ADD KEY `sites_groupId_fk` (`groupId`);

--
-- Indexes for table `stc_1_leftcolumn`
--
ALTER TABLE `stc_1_leftcolumn`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stc_1_leftcolumn_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `stc_1_leftcolumn_siteId_fk` (`siteId`);

--
-- Indexes for table `stc_1_rightcolumn`
--
ALTER TABLE `stc_1_rightcolumn`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `stc_1_rightcolumn_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  ADD KEY `stc_1_rightcolumn_siteId_fk` (`siteId`);

--
-- Indexes for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  ADD KEY `structureelements_root_idx` (`root`),
  ADD KEY `structureelements_lft_idx` (`lft`),
  ADD KEY `structureelements_rgt_idx` (`rgt`),
  ADD KEY `structureelements_level_idx` (`level`),
  ADD KEY `structureelements_elementId_idx` (`elementId`);

--
-- Indexes for table `structures`
--
ALTER TABLE `structures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `structures_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `supertableblocks`
--
ALTER TABLE `supertableblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supertableblocks_ownerId_idx` (`ownerId`),
  ADD KEY `supertableblocks_fieldId_idx` (`fieldId`),
  ADD KEY `supertableblocks_typeId_idx` (`typeId`),
  ADD KEY `supertableblocks_sortOrder_idx` (`sortOrder`),
  ADD KEY `supertableblocks_ownerSiteId_idx` (`ownerSiteId`);

--
-- Indexes for table `supertableblocktypes`
--
ALTER TABLE `supertableblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supertableblocktypes_fieldId_idx` (`fieldId`),
  ADD KEY `supertableblocktypes_fieldLayoutId_idx` (`fieldLayoutId`);

--
-- Indexes for table `systemmessages`
--
ALTER TABLE `systemmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `systemmessages_key_language_unq_idx` (`key`,`language`),
  ADD KEY `systemmessages_language_idx` (`language`);

--
-- Indexes for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `taggroups_name_idx` (`name`),
  ADD KEY `taggroups_handle_idx` (`handle`),
  ADD KEY `taggroups_dateDeleted_idx` (`dateDeleted`),
  ADD KEY `taggroups_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_groupId_idx` (`groupId`);

--
-- Indexes for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD KEY `templatecacheelements_cacheId_idx` (`cacheId`),
  ADD KEY `templatecacheelements_elementId_idx` (`elementId`);

--
-- Indexes for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecachequeries_cacheId_idx` (`cacheId`),
  ADD KEY `templatecachequeries_type_idx` (`type`);

--
-- Indexes for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  ADD KEY `templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  ADD KEY `templatecaches_siteId_idx` (`siteId`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tokens_token_unq_idx` (`token`),
  ADD KEY `tokens_expiryDate_idx` (`expiryDate`);

--
-- Indexes for table `usergroups`
--
ALTER TABLE `usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_handle_unq_idx` (`handle`),
  ADD UNIQUE KEY `usergroups_name_unq_idx` (`name`);

--
-- Indexes for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  ADD KEY `usergroups_users_userId_idx` (`userId`);

--
-- Indexes for table `userpermissions`
--
ALTER TABLE `userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_name_unq_idx` (`name`);

--
-- Indexes for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  ADD KEY `userpermissions_usergroups_groupId_idx` (`groupId`);

--
-- Indexes for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  ADD KEY `userpermissions_users_userId_idx` (`userId`);

--
-- Indexes for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_uid_idx` (`uid`),
  ADD KEY `users_verificationCode_idx` (`verificationCode`),
  ADD KEY `users_email_idx` (`email`),
  ADD KEY `users_username_idx` (`username`),
  ADD KEY `users_photoId_fk` (`photoId`);

--
-- Indexes for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  ADD KEY `volumefolders_parentId_idx` (`parentId`),
  ADD KEY `volumefolders_volumeId_idx` (`volumeId`);

--
-- Indexes for table `volumes`
--
ALTER TABLE `volumes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `volumes_name_idx` (`name`),
  ADD KEY `volumes_handle_idx` (`handle`),
  ADD KEY `volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  ADD KEY `volumes_dateDeleted_idx` (`dateDeleted`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `widgets_userId_idx` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assettransformindex`
--
ALTER TABLE `assettransformindex`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `assettransforms`
--
ALTER TABLE `assettransforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorygroups`
--
ALTER TABLE `categorygroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `deprecationerrors`
--
ALTER TABLE `deprecationerrors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `drafts`
--
ALTER TABLE `drafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elementindexsettings`
--
ALTER TABLE `elementindexsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `elements`
--
ALTER TABLE `elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `elements_sites`
--
ALTER TABLE `elements_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `entrytypes`
--
ALTER TABLE `entrytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fieldgroups`
--
ALTER TABLE `fieldgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `fieldlayouts`
--
ALTER TABLE `fieldlayouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `globalsets`
--
ALTER TABLE `globalsets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `matrixcontent_defaultmatrix`
--
ALTER TABLE `matrixcontent_defaultmatrix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `plugins`
--
ALTER TABLE `plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `queue`
--
ALTER TABLE `queue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sections_sites`
--
ALTER TABLE `sections_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sitegroups`
--
ALTER TABLE `sitegroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stc_1_leftcolumn`
--
ALTER TABLE `stc_1_leftcolumn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stc_1_rightcolumn`
--
ALTER TABLE `stc_1_rightcolumn`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `structureelements`
--
ALTER TABLE `structureelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `structures`
--
ALTER TABLE `structures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supertableblocktypes`
--
ALTER TABLE `supertableblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `systemmessages`
--
ALTER TABLE `systemmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `taggroups`
--
ALTER TABLE `taggroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `templatecaches`
--
ALTER TABLE `templatecaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usergroups`
--
ALTER TABLE `usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions`
--
ALTER TABLE `userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `userpreferences`
--
ALTER TABLE `userpreferences`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `volumefolders`
--
ALTER TABLE `volumefolders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `volumes`
--
ALTER TABLE `volumes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assetindexdata`
--
ALTER TABLE `assetindexdata`
  ADD CONSTRAINT `assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `categorygroups`
--
ALTER TABLE `categorygroups`
  ADD CONSTRAINT `categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categorygroups_sites`
--
ALTER TABLE `categorygroups_sites`
  ADD CONSTRAINT `categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `craftidtokens`
--
ALTER TABLE `craftidtokens`
  ADD CONSTRAINT `craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `drafts`
--
ALTER TABLE `drafts`
  ADD CONSTRAINT `drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `drafts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `revisions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `elements_sites`
--
ALTER TABLE `elements_sites`
  ADD CONSTRAINT `elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entries`
--
ALTER TABLE `entries`
  ADD CONSTRAINT `entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `entries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `entrytypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `entrytypes`
--
ALTER TABLE `entrytypes`
  ADD CONSTRAINT `entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayoutfields`
--
ALTER TABLE `fieldlayoutfields`
  ADD CONSTRAINT `fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fieldlayouttabs`
--
ALTER TABLE `fieldlayouttabs`
  ADD CONSTRAINT `fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fields`
--
ALTER TABLE `fields`
  ADD CONSTRAINT `fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `globalsets`
--
ALTER TABLE `globalsets`
  ADD CONSTRAINT `globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocks`
--
ALTER TABLE `matrixblocks`
  ADD CONSTRAINT `matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `matrixblocktypes`
--
ALTER TABLE `matrixblocktypes`
  ADD CONSTRAINT `matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `matrixcontent_defaultmatrix`
--
ALTER TABLE `matrixcontent_defaultmatrix`
  ADD CONSTRAINT `matrixcontent_defaultmatrix_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `matrixcontent_defaultmatrix_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `migrations`
--
ALTER TABLE `migrations`
  ADD CONSTRAINT `migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `plugins` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `relations`
--
ALTER TABLE `relations`
  ADD CONSTRAINT `relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `revisions`
--
ALTER TABLE `revisions`
  ADD CONSTRAINT `revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `sections_sites`
--
ALTER TABLE `sections_sites`
  ADD CONSTRAINT `sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shunnedmessages`
--
ALTER TABLE `shunnedmessages`
  ADD CONSTRAINT `shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `sitegroups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stc_1_leftcolumn`
--
ALTER TABLE `stc_1_leftcolumn`
  ADD CONSTRAINT `stc_1_leftcolumn_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stc_1_leftcolumn_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `stc_1_rightcolumn`
--
ALTER TABLE `stc_1_rightcolumn`
  ADD CONSTRAINT `stc_1_rightcolumn_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stc_1_rightcolumn_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `structureelements`
--
ALTER TABLE `structureelements`
  ADD CONSTRAINT `structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `structures` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `supertableblocks`
--
ALTER TABLE `supertableblocks`
  ADD CONSTRAINT `supertableblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `supertableblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `supertableblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `supertableblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `supertableblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `supertableblocktypes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `supertableblocktypes`
--
ALTER TABLE `supertableblocktypes`
  ADD CONSTRAINT `supertableblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `supertableblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `taggroups`
--
ALTER TABLE `taggroups`
  ADD CONSTRAINT `taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tags_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecacheelements`
--
ALTER TABLE `templatecacheelements`
  ADD CONSTRAINT `templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `elements` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecachequeries`
--
ALTER TABLE `templatecachequeries`
  ADD CONSTRAINT `templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `templatecaches` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `templatecaches`
--
ALTER TABLE `templatecaches`
  ADD CONSTRAINT `templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `usergroups_users`
--
ALTER TABLE `usergroups_users`
  ADD CONSTRAINT `usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_usergroups`
--
ALTER TABLE `userpermissions_usergroups`
  ADD CONSTRAINT `userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpermissions_users`
--
ALTER TABLE `userpermissions_users`
  ADD CONSTRAINT `userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userpreferences`
--
ALTER TABLE `userpreferences`
  ADD CONSTRAINT `userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_fk` FOREIGN KEY (`id`) REFERENCES `elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `assets` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `volumefolders`
--
ALTER TABLE `volumefolders`
  ADD CONSTRAINT `volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `volumefolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `volumes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `volumes`
--
ALTER TABLE `volumes`
  ADD CONSTRAINT `volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `widgets`
--
ALTER TABLE `widgets`
  ADD CONSTRAINT `widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
